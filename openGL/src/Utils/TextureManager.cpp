#include "Utils/TextureManager.h"
#include "Application/Scene.h"

using namespace Utils;

const std::string TextureManager::defaultTextures[] = {"img/treeTrunk.png"};
std::map<TexID,GLint> TextureManager::textures;

void TextureManager::init(void)
{
    ilInit();
    iluInit();
    ilutInit();
    ilutRenderer(ILUT_OPENGL);
}

void TextureManager::loadTexture(TexID id, const char* fileName) 
{
    GLint result = ilutGLLoadImage((ILstring)fileName);
    if(result == 0)
	{
        return;
	}

    textures[id] = result;

    glBindTexture(GL_TEXTURE_2D, result);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); // Linear Filtering
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); // Linear Filtering

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
    

void TextureManager::loadAll(void) 
{
    loadTexture(TexID::METAL_LEGS, "img/metalLegs.png");
    loadTexture(TexID::METAL_WHEEL, "img/metalWheel.png");
	loadTexture(TexID::LINE, "img/line.png");
    loadTexture(TexID::PRODUCT, "img/box.png");
    loadTexture(TexID::METAL_SIDE, "img/metalSide.png");
    loadTexture(TexID::FACTORY_WALL, "img/factoryWall.png");
	loadTexture(TexID::FACTORY_SIDE_WALL, "img/factorySideWall.png");
    loadTexture(TexID::ROBOT_ARM, "img/robotArm.png");

	loadTexture(TexID::WHEEL, "img/wheel.png");

	loadTexture(TexID::R2D2_TOP, "img/r2d2Top.png");
	loadTexture(TexID::R2D2_CENTER, "img/r2d2Center.png");
	loadTexture(TexID::R2D2_BOTTOM, "img/r2d2Bottom.png");

	loadTexture(TexID::FLOOR, "img/floor.png");

	loadTexture(TexID::SKYBOX_LEFT, "img/skybox/left.png");
	loadTexture(TexID::SKYBOX_RIGHT, "img/skybox/right.png");
	loadTexture(TexID::SKYBOX_TOP, "img/skybox/top.png");
	loadTexture(TexID::SKYBOX_BOTTOM, "img/skybox/bottom.png");
	loadTexture(TexID::SKYBOX_FRONT, "img/skybox/front.png");
	loadTexture(TexID::SKYBOX_BACK, "img/skybox/back.png");
}

GLint TextureManager::getTexture(TexID id) 
{
    auto result = textures.find(id);
    if(result == textures.end())
	{
        return 0;
	}

    return result->second;
}

void TextureManager::createDepthTexture() {

    GLuint renderedTexture;
    glGenTextures(1, &renderedTexture);

    glBindTexture(GL_TEXTURE_2D, renderedTexture);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT24, SMAP, SMAP, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    textures[TexID::DEPTH] = renderedTexture;

    glBindTexture(GL_TEXTURE_2D, 0);
}
