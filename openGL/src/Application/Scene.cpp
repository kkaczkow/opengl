#include "Application/Scene.h"
#include <thread>

Scene *Scene::instance = Scene::makeInst();
GLsizei Scene::width = 1280;
GLsizei Scene::height = 960;
float distanceToFloor = 79.35f;

void Scene::init(void) 
{
    GLfloat light_position2[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat laser_diffuse[] = {1.0f, 0.0f, 0.0f, 1.0f};
    GLfloat laser_direction[] = {0.0f, 0.0f, -1.0f, 0.0f};
	

    //GL_LIGHT1 properties
    light_ambient[0] = 0.8f;
    light_ambient[1] = 0.8f;
    light_ambient[2] = 0.8f;
    light_ambient[3] = 1.0f;
    light_specular[0] = 0.4f;
    light_specular[1] = 0.4f;
    light_specular[2] = 0.6f;
    light_specular[3] = 1.0f;
    light_diffuse[0] = 0.5f;
    light_diffuse[1] = 0.5f;
    light_diffuse[2] = 0.7f;
    light_diffuse[3] = 1.0f;

    /*whiteMat[0] = 0.1f;
    whiteMat[1] = 0.1f;
    whiteMat[2] = 0.15f; 
    whiteMat[3] = 1.0f;
    blackMat[0] = 0.05f;
    blackMat[1] = 0.05f;
    blackMat[2] = 0.1f; 
    blackMat[3] = 1.0f;*/

	whiteMat[0] = 0.25f;
    whiteMat[1] = 0.25f;
    whiteMat[2] = 0.3f; 
    whiteMat[3] = 1.0f;
    blackMat[0] = 0.3f;
    blackMat[1] = 0.15f;
    blackMat[2] = 0.4f; 
    whiteMat[3] = 1.0f;

    glLightfv(GL_LIGHT2, GL_POSITION, light_position2);
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, laser_direction);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, laser_diffuse);
    glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, 20.0f);
    glLightf (GL_LIGHT2, GL_SPOT_EXPONENT, 90.0);
    
	x_pos = 0.0f;
	y_pos = 0.0f;
	z_pos = 0.0f;
	MOVE_SPEED = 1.0f;
    curr_horizontal_angle = 45.0f;
    curr_vertical_angle = 0.0f;
	camera_type_free = true;
	xRot = 45.0f;
    yRot = 45.0f;
    distance = -10.0f;

	//create skybox
	skybox = Skybox::loadSkybox();
	terrain = Terrain::loadTerrain("img/heightmap.bmp");
    
    //create factory
    coords[0] = coords[1] = coords[2] = 0.0f;
    factory = FactoryPtr(new Factory(coords, 0.01f));
    
    //create a robot
    coords[0] = coords[1] = coords[2] = 0.0f;
    robot = RobotPtr(new Robot(coords, 0.01f, factory->getBoxSize()));
	
    //set factory position relative to just created robot
    coords[0] = coords[1] = 0.0f;
    coords[2] = - glm::abs(factory->getFrontBeltEndPoint()) - glm::abs(robot->getArmRange());
    factory->setMiddlePoint(coords);
	
	//create rear belt
    coords[2] = robot->getArmRange() + 1.8f;
    coords[0] = coords[1] = 0.0f;
    auto ptr = BeltPtr(new Belt(coords, 0.01f, false, false));

    //set belt position relative to just robot
    ptr->setMiddlePoint(coords);
    belts.push_back(ptr);

	//create right belt
    coords[1] = coords[2] = 0.0f;
    coords[0] = robot -> getArmRange();
    ptr = BeltPtr(new Belt(coords, 0.01f, true, false));

    //set belt position relative to just robot
    coords[2] = ptr->getLengthToEndPoint();
    ptr->setMiddlePoint(coords);
    belts.push_back(ptr);
	
	
    //create left belt
    coords[1] = coords[2] = 0.0f;
    coords[0] = - robot -> getArmRange();
    ptr = BeltPtr(new Belt(coords, 0.01f, false, true));

    //set belt position relative to just robot
    coords[2] = ptr->getLengthToEndPoint();
    ptr->setMiddlePoint(coords);
    belts.push_back(ptr);
	
    for(auto &b : belts) 
	{
        b->setLineTex
			(
            Utils::TextureManager::getTexture(Utils::TexID::METAL_WHEEL),
            Utils::TextureManager::getTexture(Utils::TexID::METAL_SIDE),
            Utils::TextureManager::getTexture(Utils::TexID::LINE)
            );
    }

    factory->setLineTex
		(
        Utils::TextureManager::getTexture(Utils::TexID::METAL_WHEEL),
        Utils::TextureManager::getTexture(Utils::TexID::METAL_SIDE),
        Utils::TextureManager::getTexture(Utils::TexID::LINE)
        );

    factory->setMainWallTex(Utils::TextureManager::getTexture(Utils::TexID::FACTORY_WALL));
	factory->setSideWallTex(Utils::TextureManager::getTexture(Utils::TexID::FACTORY_SIDE_WALL));

    glShadeModel( GL_SMOOTH );
    glClearColor(0.0f, 0.2f, 0.8f, 1.0f);
    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT1 );
    glEnable( GL_NORMALIZE );
    glEnable(GL_TEXTURE_2D);

    glDepthFunc( GL_LEQUAL );
    glEnable( GL_DEPTH_TEST );
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	GLfloat fogColor[4] = {0.7,0.7,0.7,0.2}; // Kolor mg�y
	glFogi(GL_FOG_MODE, GL_EXP);
	glFogi(GL_FOG_HINT, GL_NICEST);
	glFogfv(GL_FOG_COLOR, fogColor); // Kolor mg�y
	glFogf(GL_FOG_DENSITY, 0.006f); // G�sto��
	glFogf(GL_FOG_START, -100.0f); // Pocz�tek
	glFogf(GL_FOG_END, 100.0f); // Koniec

    doShadows = true;
	doFog = false;
    initMatrices();

}

Scene *Scene::makeInst(void) 
{
    return new Scene();
}

void Scene::displayObjects() 
{
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();
    {
		glPushMatrix();
		{

            glEnable(GL_LIGHT1);
			glEnable(GL_LIGHTING);
			skybox->draw();
			terrain->renderTerrain();

			glPushMatrix();
				glTranslatef(lightPosition[0],lightPosition[1],lightPosition[2]);
				GLUquadricObj * quadratic=gluNewQuadric();
				gluSphere(quadratic,1.3f,32,32);
			glPopMatrix();

			if(doFog)
			{
				glEnable(GL_FOG);
			}
			else 
			{
				glDisable(GL_FOG);
			}

			glPushMatrix();
			{
				glTranslatef(0.0f, -distanceToFloor, 0.0f);
				glEnable(GL_TEXTURE_2D);
				
				glScalef(4.0f, 4.0f, 4.0f);
				//drawCoordLines();
				
				for(auto &b : belts) 
				{
					b->display();
				}
				robot-> display();
				factory->display();
				

				glDisable(GL_TEXTURE_2D);  
				glDisable(GL_LIGHTING);
			}
			glPopMatrix();

		}
		glPopMatrix();

		
	}
	glPopMatrix();


}

void Scene::display(void) 
{
    Scene *sc = Scene::getInstance();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    

    if(sc->doShadows)
        sc->displayFromLightPOV();

    glClear( GL_DEPTH_BUFFER_BIT);
    sc->displayFromCameraPOV(); //render scene

    glFlush();
    glutSwapBuffers();

}

void Scene::reshape(GLsizei w, GLsizei h) 
{

    if( h > 0 && w > 0 ) 
	{

        glViewport( 0, 0, w, h );
        glMatrixMode( GL_PROJECTION );
        glLoadIdentity();

        width = w; height = h;

        GLfloat aspect = (GLfloat)w/(GLfloat)h;

        if( w <= h ) 
		{
            gluPerspective(45.0f, aspect, 0.1f, 30.0f);
        }
        else 
		{
            gluPerspective(45.0f, aspect ,0.1f, 30.0f);
        }

        glMatrixMode( GL_MODELVIEW );
    }
}

void Scene::drawCoordLines(void) 
{

    float red[] = {1.0f, 0.0f, 0.0f, 1.0f};
    float green[] = {0.0f, 1.0f, 0.0f, 1.0f};
    float blue[] = {0.0f, 0.0f, 1.0f, 1.0f};
    float old[] = {1.0f, 2.0f, 3.0f, 1.0f};
    glLineWidth(2.5); 
    glGetMaterialfv(GL_FRONT,GL_DIFFUSE, old);
    glMaterialfv( GL_FRONT, GL_DIFFUSE, red );
    glBegin(GL_LINES);
    glNormal3f(1.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(3.0f, 0.0f, 0.0f);
    glEnd();

    glMaterialfv( GL_FRONT, GL_DIFFUSE, green );
    glBegin(GL_LINES);
    glNormal3f(0.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 3.0f, 0.0f);
    glEnd();

    glMaterialfv( GL_FRONT, GL_DIFFUSE, blue );
    glBegin(GL_LINES);
    glNormal3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 3.0f);
    glEnd();

    glMaterialfv( GL_FRONT, GL_DIFFUSE, old );
}

void Scene::initMatrices(void) {

	/*
	lightPosition[0] = 0.0f;
	lightPosition[1] = -distanceToFloor + 50.0f;
    lightPosition[2] = -35.0f;
	lightPosition[3] = 1.0f;*/

	lightPosition[0] = 0.0f;
	lightPosition[1] = -distanceToFloor + 50.0f;
    lightPosition[2] = -35.0f;
	lightPosition[3] = 1.0f;

    //Load identity modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    //Calculate & save matrices
	glPushMatrix();
	
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.25f, 2 * sqrt(3.0) * SKYBOX_SIZE);
	glGetFloatv(GL_MODELVIEW_MATRIX, cameraProjection);
	
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, distance,
				0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f);
	glGetFloatv(GL_MODELVIEW_MATRIX, cameraView);
	
	glLoadIdentity();
	gluPerspective(30.0f, (GLfloat)width/(GLfloat)height, 30.0f, sqrt(3.0) * SKYBOX_SIZE/3.0);
	glGetFloatv(GL_MODELVIEW_MATRIX, lightProjection);
	
	glLoadIdentity();
	gluLookAt(lightPosition[0], lightPosition[1], lightPosition[2],
				0.0f, -distanceToFloor, 0.0f,
				0.0f, 1.0f, 0.0f);
	glGetFloatv(GL_MODELVIEW_MATRIX, lightView);
	
	glPopMatrix();
}

void Scene::displayFromLightPOV() {

    glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(lightProjection);

	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(lightView);

    glViewport(0, 0, SMAP, SMAP);
	glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
	//Disable color writes, and use flat shading for speed
	glShadeModel(GL_SMOOTH);
    glDisable(GL_LIGHTING);
	glColorMask(0, 0, 0, 0);
    
    displayObjects();
    
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glBindTexture(GL_TEXTURE_2D,Utils::TextureManager::getTexture(Utils::TexID::DEPTH));
    glCopyTexSubImage2D(GL_TEXTURE_2D,0,0,0,0,0,SMAP,SMAP);

    glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);
	glColorMask(1, 1, 1, 1);
    glCullFace(GL_BACK);
	glDisable(GL_CULL_FACE);
}

void Scene::displayFromCameraPOV() {

    //set up matrices
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.25f, 2 * sqrt(3.0) * SKYBOX_SIZE);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();

    //set up camera
    if(camera_type_free)
    {
        gluLookAt(0, 0, 0,sin(curr_horizontal_angle)*cos(curr_vertical_angle), 
            sin(curr_vertical_angle), -cos(curr_horizontal_angle)*cos(curr_vertical_angle),0.0f, 1.0f,  0.0f);
        glTranslatef(-x_pos,-y_pos,-z_pos);
    }
    else 
    {
        glTranslatef(0.0f, -distanceToFloor, 0.0f);
        glTranslatef(0.0f, distanceToFloor, distance);
        glRotatef(xRot, 1.0, 0.0, 0.0);
        glRotatef(yRot, 0.0, 1.0, 0.0);
        glTranslatef(0.0f, distanceToFloor, 0.0f);
    }

    glViewport(0, 0, width, height);
    glGetFloatv(GL_MODELVIEW_MATRIX, cameraView);

    if(doShadows) {//draw with shadows

	    //Use dim light to represent shadowed areas
        setDimLight();
    
	    glEnable(GL_LIGHT1);
	    glEnable(GL_LIGHTING);

        //draw scene with dim light, 2nd pass
        glEnable(GL_TEXTURE_2D);
        skybox->draw();
        displayObjects();
        glDisable(GL_TEXTURE_2D);  

        //prepare light for 3rd pass
        setNormalLight();

        //calculate matrices for shadows calculation
        static float fOffsetX = 0.5f + (0.5f / SMAP);
        static float fOffsetY = 0.5f + (0.5f / SMAP);
        static glm::mat4 bias(      0.5f, 0.0f, 0.0f, 0.0f,
								    0.0f, 0.5f, 0.0f, 0.0f,
								    0.0f, 0.0f, 0.5f, 0.0f,
								    fOffsetX, fOffsetY, 0.5f, 1.0f);
        static glm::mat4 lightProj = glm::make_mat4x4(lightProjection);
        static glm::mat4 lightV = glm::make_mat4x4(lightView);

        static glm::mat4 mat = bias*lightProj*lightV;

        static GLfloat eyePlaneS[] = {mat[0][0], mat[1][0], mat[2][0], mat[3][0]};
        static GLfloat eyePlaneT[] = {mat[0][1], mat[1][1], mat[2][1], mat[3][1]};
        static GLfloat eyePlaneR[] = {mat[0][2], mat[1][2], mat[2][2], mat[3][2]};
        static GLfloat eyePlaneQ[] = {mat[0][3], mat[1][3], mat[2][3], mat[3][3]};


        glActiveTextureARB(GL_TEXTURE0_ARB);
        glEnable(GL_TEXTURE_2D);

        glActiveTextureARB(GL_TEXTURE1_ARB);
        glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
        glTexGenfv(GL_S, GL_EYE_PLANE, eyePlaneS);
        glEnable(GL_TEXTURE_GEN_S);

        glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
        glTexGenfv(GL_T, GL_EYE_PLANE, eyePlaneT);
        glEnable(GL_TEXTURE_GEN_T);

        glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
        glTexGenfv(GL_R, GL_EYE_PLANE, eyePlaneR);
        glEnable(GL_TEXTURE_GEN_R);

        glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
        glTexGenfv(GL_Q, GL_EYE_PLANE, eyePlaneQ);
        glEnable(GL_TEXTURE_GEN_Q);
    
        //Bind & enable shadow map texture
	    glBindTexture(GL_TEXTURE_2D, Utils::TextureManager::getTexture(Utils::TexID::DEPTH));
	    glEnable(GL_TEXTURE_2D);

	    //Enable shadow comparison
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL);
	    //Shadow comparison should generate an INTENSITY result
	    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY);
    
        //set up alpha test to cut off shadowed pixels
        glAlphaFunc(GL_GEQUAL, 0.8f);
	    glEnable(GL_ALPHA_TEST);

        //run the 3rd pass rendering with normal light
        displayObjects();
    

        glActiveTextureARB(GL_TEXTURE1_ARB);
        glDisable(GL_ALPHA_TEST);
        glDisable(GL_TEXTURE_GEN_S);
        glDisable(GL_TEXTURE_GEN_T);
        glDisable(GL_TEXTURE_GEN_R);
        glDisable(GL_TEXTURE_GEN_Q);

        glDisable(GL_TEXTURE_2D);

        glDisable(GL_LIGHTING);
    } else {//draw without shadows
        //set up lights
        setNormalLight();

        glEnable(GL_LIGHT1);
	    glEnable(GL_LIGHTING);

        glEnable(GL_TEXTURE_2D);
        skybox->draw();
        displayObjects();
        glDisable(GL_TEXTURE_2D);  
        glDisable(GL_LIGHTING);
    }
    
    glPopMatrix();
}

void Scene::setDimLight(void) {
    glLightfv(GL_LIGHT1, GL_POSITION, lightPosition);
	glLightfv(GL_LIGHT1, GL_AMBIENT, whiteMat);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, whiteMat);
	glLightfv(GL_LIGHT1, GL_SPECULAR, blackMat);
}

void Scene::setNormalLight(void) {
    glLightfv(GL_LIGHT1, GL_POSITION, lightPosition);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_specular);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_diffuse);
}

