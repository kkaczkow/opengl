#include "Application/App.h"

void App::run(int argc, char** argv) {

    glutInit( &argc, argv );

    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowPosition( 0, 0 );
    glutInitWindowSize( Scene::width, Scene::height );

    glutCreateWindow( "GPOB: OpenGL" );

    Utils::TextureManager::init();
    Utils::TextureManager::loadAll();
    Utils::TextureManager::createDepthTexture();

    Scene::getInstance()->init();

    glutDisplayFunc( Scene::display );
    glutReshapeFunc( Scene::reshape );
    glutIdleFunc( SceneLogic::animStep );
    glutKeyboardFunc( SceneLogic::keyDownFunc );
    glutKeyboardUpFunc( SceneLogic::keyUpFunc );
	glutMouseFunc( SceneLogic::mouseKey ); 
    glutMotionFunc( SceneLogic::mouseMove ); 

    glutMainLoop();
}
