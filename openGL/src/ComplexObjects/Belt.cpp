#include "ComplexObjects/Belt.h"

Belt::Belt(float *middlePoint, GLfloat speed, GLboolean ifRightLine, GLboolean ifLeftLine)
{
    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];
    

    this->dx = speed;

    this->running = false;

    //middle line
    linePos[0] = 0.0f;
    linePos[1] = -0.25f;
    linePos[2] = 0.0f;
    lines.push_back(AssemblyLine(linePos, 10, 4, 0.8f, dx));

	//middle line legs
    beltLegsCreate(0, true, true);
    beltLegsCreate(0, true, false);
    beltLegsCreate(0, false, true);
    beltLegsCreate(0, false, false);

    //rear line
    linePos[0] = 0.0f;
    linePos[1] = -0.25f;
    linePos[2] = -1.0f - 2.25f * lines[0].getRadius();// length, 2* line radius and 0.25 correctness factor
    lines.push_back(AssemblyLine(linePos, 10, 4, 0.8f, dx));

	//rear line legs
    beltLegsCreate(1, true, true);
    beltLegsCreate(1, true, false);
    beltLegsCreate(1, false, true);
    beltLegsCreate(1, false, false);

    //front line
    linePos[0] = 0.0f;
    linePos[1] = -0.25f;
    linePos[2] = -linePos[2];
    lines.push_back(AssemblyLine(linePos, 10, 4, 0.8f, dx));

    //front line legs
    beltLegsCreate(2, true, true);
    beltLegsCreate(2, true, false);
    beltLegsCreate(2, false, true);
    beltLegsCreate(2, false, false);

	this->ifLeftLine = ifLeftLine;
	this->ifRightLine = ifRightLine;

	
}


Belt::~Belt(void)
{
}

void Belt::display(void) 
{
    glPushMatrix();
	
	if(ifRightLine) 
	{
		glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(0.0f, middlePoint[1], middlePoint[0]+1.8f);
	}
	else if(ifLeftLine)
	{
		glRotatef(270.0f, 0.0f, 1.0f, 0.0f);
		glTranslatef(0.0f, middlePoint[1], -middlePoint[0]+1.8f);
	}
	else
	{
		glTranslatef(middlePoint[0], middlePoint[1], middlePoint[2]);
	}

    glPushMatrix();
    {
        glScalef(4.5f/7.0f * 1.5f, 1.0f, 1.0f);
        for(auto &l : lines) 
		{
            l.display();
        }
    }
    glPopMatrix();

    for(auto &p : products) 
	{
        p->display();
    }

	for(auto &l : boxLegs) 
	{
        l.display();
    }

	for(auto &l : cylinderLegs) 
	{
        l.display();
    }

    glPopMatrix();
}

void Belt::step(void) 
{

    if(!running) return;

    for(auto &l : lines) 
	{
        l.step();
    }

    for(auto p = products.begin(); p != products.end();) 
	{
        //if front cylinder of front assembly line was not reached move the box
        if((*p)->middlePoint[2] < (lines[2].getMiddlePoint()[2] + 1.0f/2.0f) ) 
		{
            (*p)->middlePoint[2] += dx;
            ++p;
        }
        //otherwise stop the whole belt
        else 
		{

            p = products.erase(p);
            if(products.empty())
			{
                running = false;
			}
        }
    }

}

void Belt::putProduct(std::shared_ptr<BoxData> b) 
{
    b->middlePoint[2] = this->lines[1].getMiddlePoint()[2] - 0.5f; //middle point minus half length
    products.push_back(b);
    running = true;
}

void Belt::removeProduct(void) 
{
    if(running || products.empty()) 
	{
		return;
	}

    products.pop_front();

    running = false;
}

void Belt::setLineTex(GLint cylinderBaseTex, GLint cylinderSideTex, GLint plankTexID) 
{
    for(auto &l : lines) 
	{
        l.setCylinderTexure(cylinderBaseTex, cylinderSideTex);
        l.setPlankTexture(plankTexID);
    }
}

GLfloat Belt::getLengthToEndPoint(void) 
{
    return glm::abs(lines[1].getMiddlePoint()[2]) + 0.5f;
}

void Belt::beltLegsCreate (unsigned i, bool leftSide, bool rearCylinder) 
{
	//horizontal part of leg
    legPos[0] = leftSide ? -0.5f : 0.5f;
	legPos[1] = -0.25f;
	legPos[2] = (rearCylinder ? lines[i].getRearCylinder()[2] :
		lines[i].getFrontCylinder()[2]) + lines[i].getMiddlePoint()[2];
	
	boxLegs.push_back(BoxData(legPos, 0.1f, 0.05f, 0.05f, 
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));
	
	//vertical part of leg
	legPos[0] = leftSide ? -0.5f - 0.075f : 0.5f + 0.075f;
	legPos[1] = (-1.1 + 0.25f)/2.0f - 0.25f;
	legPos[2] = (rearCylinder ? lines[i].getRearCylinder()[2] :
		lines[i].getFrontCylinder()[2]) + lines[i].getMiddlePoint()[2];

	boxLegs.push_back(BoxData(legPos, 0.05f, glm::abs(-1.1 + 0.25f) + 0.05f , 0.05f,
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));


	// endings
	legPos[0] = leftSide ? -0.5f - 0.075f : 0.5f + 0.075f;
	legPos[1] = -1.0f;
	legPos[2] = (rearCylinder ? lines[i].getRearCylinder()[2] :
		lines[i].getFrontCylinder()[2]) + lines[i].getMiddlePoint()[2];

	cylinderLegs.push_back(CylinderData(legPos, 0.09f, 0.08f , 15,
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS),
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));
};