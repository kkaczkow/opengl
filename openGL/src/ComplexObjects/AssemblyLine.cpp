#include "ComplexObjects/AssemblyLine.h"
#include <tuple>

AssemblyLine::AssemblyLine(GLfloat *middlePoint, unsigned upperPlanks, unsigned arcPlanks, GLfloat fill, GLfloat speed)
{
    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];

    unsigned num = upperPlanks;
    unsigned arcSegments = arcPlanks;

    this->length = 1.0f; //default length of assemblyline

    const GLfloat segmentDepth = length / (GLfloat)num; //depth of single box segment
    const GLfloat plankHeight = 0.2f * segmentDepth; //height of plank
    const GLfloat segmentFill = fill; //% of plank's depth filled
    const GLuint cylinderSegments = 15; //cylinder precision

    this->dx = speed;
    this->r = ((GLfloat)arcSegments * (length / (GLfloat)num))/ 3.14f; //calculate r according to plank number on arc
    this->da = (180.0f*dx)/(3.14f*r);//calctulate angle change with respect to dx
    this->running = true;
    this->surface = r + plankHeight;

    const GLfloat arcLength = 3.14 * r;
    //position of furthest plank on top, z coord is calculated as the middle of first upper plank
    GLfloat plankPos[] = {0.0f, r, -length / 2.0f + segmentDepth * (segmentFill * 0.5f)};
    //position of phase 2 planks
    GLfloat cylinderPos[] = {0.0f, r, length/2.0f};

    GLfloat cylRot[] = {0.0f, 0.0f, 90.0f};
    GLfloat cylMidPoint[] = {0.0f, 0.0f, -length/2.0f};

    //rear cylinder
    cylis.push_back
		(
        std::make_tuple
		(
        CylinderData(
        cylMidPoint, //midpoint
        1.0f, //height
        r - plankHeight/2.0f, //radius
        cylinderSegments, //segments
        0, //default texture 
        0, //default texture 
        cylRot) //rotation
        )
        );

    //front cylinder
    cylMidPoint[2] = length/2.0f;
    cylis.push_back
		(
        std::make_tuple
		(
        CylinderData(
        cylMidPoint, //midpoint
        1.0f,//height
        r - plankHeight/2.0f, //radius
        cylinderSegments, //segments
        0, //default texture 
        0, //default texture 
        cylRot)//rotation
        )
        );


    //phase 1 data (TOP PLANKS)
    for(unsigned i = 0; i < num; ++i) 
	{
        data.push_back
			(
            std::make_tuple
			(
            BoxData(
            plankPos,//midpoint
            1.0f,//width
            plankHeight,//height 
            segmentFill * segmentDepth,//depth
            0 //default texture 
            ),
            0.0f,//angle around cylinder
            BoxPhase::PHASE1//phase
            )
            );
        plankPos[2] += segmentDepth;
    }

    //phase 2 (FRONT ARC PLANKS)
    GLfloat angle = segmentDepth * segmentFill * 0.5f;
    for(unsigned i = 0; i < arcSegments; ++i) 
	{
        data.push_back(
            std::make_tuple
			(
            BoxData(
            cylinderPos,//midpoint
            1.0f,//width
            plankHeight,//height 
            segmentFill * segmentDepth,//depth
            0), //default texture 
            (angle * 180.0f)/arcLength,//angle around cylinder
            BoxPhase::PHASE2)//phase
            );
        angle += segmentDepth;
    }

    //phase 3 data (BOTTOM PLANKS)
    //position of nearest plank on bottom, z coord is calculated as the middle of first bottom plank
    plankPos[0] = 0.0f;
    plankPos[1] = -r;
    plankPos[2] = length/2.0f - segmentDepth * segmentFill * 0.5f;
    GLfloat rot[] = {0.0f, 180.0f, 0.0f};
    for(unsigned i = 0; i < num; ++i) 
	{
        data.push_back
			(
            std::make_tuple
			(
            BoxData(
            plankPos,//midpoint
            1.0f,//width
            plankHeight, //height 
            segmentFill * segmentDepth,//depth
            0, //default texture 
            rot),//rotation of plank around its middle point
            0.0f,//angle around cylinder
            BoxPhase::PHASE3)//phase
            );
        plankPos[2] -= segmentDepth;
    }

    //phase4 (REAR ARC PLANKS)
    cylinderPos[0] = 0.0f;
    cylinderPos[1] = -r;
    cylinderPos[2] = -length/2.0f;
    angle = segmentDepth * segmentFill * 0.5f;
    for(unsigned i = 0; i < arcSegments; ++i) 
	{
        data.push_back
			(
            std::make_tuple
			(
            BoxData(
            cylinderPos,//midpoint
            1.0f, //width
            plankHeight, //height 
            segmentFill * segmentDepth,//depth
            0, //default texture 
            rot),//rotation of plank around its middle point
            (angle * 180.0f)/arcLength,//angle around cylinder
            BoxPhase::PHASE4)//phase
            );
        angle += segmentDepth;
    }
}


AssemblyLine::~AssemblyLine(void)
{
}

void AssemblyLine::display(void) 
{
    glPushMatrix();
    glTranslatef(middlePoint[0], middlePoint[1], middlePoint[2]);
    for(auto &d : data) 
	{//draw planks
        glPushMatrix();
        {
            //planks in phase 2 or phase 4 need to be rotated
            //around respective cylinder
            if(std::get<2>(d) == BoxPhase::PHASE2) 
			{
                glTranslatef(0.0f, 0.0f, length/2.0f);
                glRotatef(std::get<1>(d), 1.0f, 0.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, -length/2.0f);
            } else if(std::get<2>(d) == BoxPhase::PHASE4) 
			{
                glTranslatef(0.0f, 0.0f, -length/2.0f);
                glRotatef(std::get<1>(d), 1.0f, 0.0f, 0.0f);
                glTranslatef(0.0f, 0.0f, length/2.0f);
            }
            std::get<0>(d).display();
        }
        glPopMatrix();
    }


    for(auto &c : cylis) 
	{//draw cylinders
        std::get<0>(c).display();
    }

    glPopMatrix();
}

void AssemblyLine::step(void) 
{

    if(!running) return;

    BoxData *box;
    float *angle;
    for(auto &d : data) 
	{

        box = &(std::get<0>(d));

        if(std::get<2>(d) == BoxPhase::PHASE1) 
		{
            boxPhase1(d);
        } 
		else if(std::get<2>(d) == BoxPhase::PHASE2) 
		{
            boxPhase2(d);
        } 
		else if(std::get<2>(d) == BoxPhase::PHASE3) 
		{
            boxPhase3(d);
        } 
		else if(std::get<2>(d) == BoxPhase::PHASE4) 
		{
            boxPhase4(d);
        }
    }

    for(auto &c : cylis) 
	{
        angle = std::get<0>(c).getSelfRotation();
        angle[0] += da;
        std::get<0>(c).setSelfRotation(angle);
    }
}

void AssemblyLine::boxPhase1(BoxTuple &data) 
{
    float *p = std::get<0>(data).getMiddlePoint();

    p[2] += dx;
    //check if reached phase change point and new phase
    //of animation should be handled
    if(p[2] > length/2.0f) 
	{
        p[2] = length/2.0f;//set the coordinate
        std::get<0>(data).setMiddlePoint(p);//set new middle point
        std::get<2>(data) = BoxPhase::PHASE2;
        boxPhase2(data);//proceed to phase 2
        return;
    }

    std::get<0>(data).setMiddlePoint(p);
}

void AssemblyLine::boxPhase2(BoxTuple &data)
{
    std::get<1>(data) += da;

    //check if reached phase change point and new phase
    //of animation should be handled
    if(std::get<1>(data) > 180.0f) 
	{
        std::get<1>(data) = 180.0f;
        std::get<2>(data) = BoxPhase::PHASE3;
        //set box coordinates after transition
        float *p = std::get<0>(data).getMiddlePoint();
        p[1] -= 2.0f * r;
        std::get<0>(data).setMiddlePoint(p);
        //set rotation coords after transition
        p = std::get<0>(data).getSelfRotation();
        p[1] = 180.0f;
        std::get<0>(data).setSelfRotation(p);

        boxPhase3(data);
        return;
    }
}

void AssemblyLine::boxPhase3(BoxTuple &data) 
{
    float *p = std::get<0>(data).getMiddlePoint();

    p[2] -= dx;

    //check if reached phase change point and new phase
    //of animation should be handled
    if(p[2] <-length/2.0f ) 
	{
        p[2] = -length/2.0f;//set the coordinate
        std::get<0>(data).setMiddlePoint(p);
        std::get<1>(data) = 0.0f;
        std::get<2>(data) = BoxPhase::PHASE4;
        boxPhase4(data);//as we're in p4 at this point handle it
        return;
    }

    std::get<0>(data).setMiddlePoint(p);
}

void AssemblyLine::boxPhase4(BoxTuple &data)
{
    std::get<1>(data) += da;

    //check if reached phase change point and new phase
    //of animation should be handled
    if(std::get<1>(data) > 180.0f ) 
	{
        std::get<1>(data) = 0.0f;
        std::get<2>(data) = BoxPhase::PHASE1;
        //set box coordinates after transition
        float *p = std::get<0>(data).getMiddlePoint();
        p[1] += 2.0f * r; // (-2.0f*p[1])/2.0f
        std::get<0>(data).setMiddlePoint(p);
        //set rotation coords after transition
        p = std::get<0>(data).getSelfRotation();
        p[1] = 0.0f;
        std::get<0>(data).setSelfRotation(p);

        boxPhase1(data);
        return;
    }
}

void AssemblyLine::setCylinderTexure(GLint baseTex, GLint sideTex) 
{
    for(auto &c: cylis) 
	{
        std::get<0>(c).baseTex = baseTex;
        std::get<0>(c).sideTex = sideTex;
    }
}
void AssemblyLine::setPlankTexture(GLint texID) 
{
    for(auto &b: data) 
	{
        std::get<0>(b).tex = texID;
    }
}
