#include "common/common.h"
#include "ComplexObjects/Factory.h"
#include <tuple>

Factory::Factory(GLfloat *middlePoint, GLfloat speed)
{
    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];
    this->dx = speed;
    this->running = false;
    this->rdyToTakeBox = false;
	
	createLines();
	factoryLegCreate();
	factoryWallsCreate();
	
}


Factory::~Factory(void)
{
}

void Factory::display(void) 
{
    glPushMatrix();
	{

		glTranslatef(middlePoint[0], middlePoint[1], middlePoint[2]);
		{
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

			// draw non transparent objects
			glPushMatrix();

				glPushMatrix();
				{
					glScalef(4.5f/7.0f * 1.5f, 1.0f, 1.0f);
					for(auto &l : lines) 
					{
						l.display();
					}
				}
				glPopMatrix();

				for(auto &p : products) 
				{
					p->display();
				}

				for(auto &l : boxLegs) 
				{
					l.display();
				}
				for(auto &l : cylinderLegs) 
				{
					l.display();
				}
				int i = 0;
				
				glPushMatrix();
				{
					// draw top and bottom wall
					glScalef(1.5f, 1.5f, 1.0f);
					for(auto &b : walls) 
					{
						if(i == 0 || i == 1 )
						{
							b.display();
						}
						++i;
					}
				}
				glPopMatrix();

			glPopMatrix();

			// draw transparent objects
			glEnable(GL_BLEND);
			glDepthMask(GL_FALSE);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			glColor4d(0.9,0.9,0.9,0.9);

				glPushMatrix();
				{
					// draw left, right and rear wall
					
					glScalef(1.5f, 1.5f, 1.0f);
					for(auto &b : walls) 
					{
						if(i != 0 && i != 1 )
						{
							b.display();
						}
						++i;
					}
				}
				glPopMatrix();

			glDepthMask(GL_TRUE);
			glDisable(GL_BLEND);
		}

	}
    glPopMatrix();
}

void Factory::step(void) 
{

    if(!running)
	{
		return;
	}

    for(auto &l : lines) 
	{
        l.step();
    }

    for(auto &p : products) 
	{
        if(p->middlePoint[2] < (lines[1].getMiddlePoint()[2] + 1.0f/2.0f) )
		{
            p->middlePoint[2] += dx;
		}
        else 
		{
            p->middlePoint[2] = lines[1].getMiddlePoint()[2] + 1.0f/2.0f;
            running = false;
            rdyToTakeBox = true;
        }
    }

}

void Factory::createProduct(void) 
{

    GLfloat position[3] = {0.0f, 0.0f, -0.3f};
    GLfloat size = 2.0f * (glm::abs(position[1] - lines[0].getMiddlePoint()[1]) - lines[0].getSurface());

    products.push_back(std::shared_ptr<BoxData>(new BoxData(position, size, size, size, 
		Utils::TextureManager::getTexture(Utils::TexID::PRODUCT))));
    running = true;
}

std::shared_ptr<BoxData> Factory::getProduct(void) 
{
    if(!rdyToTakeBox) 
	{
		return std::shared_ptr<BoxData>(nullptr);
	}

    return products.front();

}

std::shared_ptr<BoxData> Factory::removeProduct(void) 
{
    if(!rdyToTakeBox) 
	{
		return std::shared_ptr<BoxData>(nullptr);
	}
    auto ret = products.front();
    products.pop_front();
    rdyToTakeBox = false;

    if(!products.empty())
	{
        running = true;
	}

    return ret;

}

void Factory::setMainWallTex(GLint texID) 
{
	int i = 0;
    for(auto &w : walls) 
	{
		if(i == 0 || i == 1 )
		{
			w.setTexture(texID);
		}
		++i;
    }
}

void Factory::setSideWallTex(GLint texID) 
{
    int i = 0;
    for(auto &w : walls) 
	{
		if(i != 0 && i != 1 )
		{
			w.setTexture(texID);
		}
		++i;
    }
}

void Factory::setLineTex(GLint cylinderBaseTex, GLint cylinderSideTex, GLint plankTexID) 
{
    for(auto &l : lines) 
	{
        l.setCylinderTexure(cylinderBaseTex, cylinderSideTex);
        l.setPlankTexture(plankTexID);
    }
}

void Factory::setProductTex(GLint texID)
{
    productTex = texID;
}

void Factory::beltLegsCreate(unsigned i, bool leftSide, bool rearCylinder) 
{

	//horizontal part of leg
    legPos[0] = leftSide ? -0.5f : 0.5f;
	legPos[1] = -0.25f;
	legPos[2] = (rearCylinder ? lines[i].getRearCylinder()[2] :
		lines[i].getFrontCylinder()[2]) + lines[i].getMiddlePoint()[2];
	
	boxLegs.push_back(BoxData(legPos, 0.1f, 0.05f, 0.05f, 
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));
	
	//vertical part of leg
	legPos[0] = leftSide ? -0.5f - 0.075f : 0.5f + 0.075f;
	legPos[1] = (-1.1 + 0.25f)/2.0f - 0.25f;
	legPos[2] = (rearCylinder ? lines[i].getRearCylinder()[2] :
		lines[i].getFrontCylinder()[2]) + lines[i].getMiddlePoint()[2];

	boxLegs.push_back(BoxData(legPos, 0.05f, glm::abs(-1.1 + 0.25f) + 0.05f , 0.05f,
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));


	// endings
	legPos[0] = leftSide ? -0.5f - 0.075f : 0.5f + 0.075f;
	legPos[1] = -1.0f;
	legPos[2] = (rearCylinder ? lines[i].getRearCylinder()[2] :
		lines[i].getFrontCylinder()[2]) + lines[i].getMiddlePoint()[2];

	cylinderLegs.push_back(CylinderData(legPos, 0.09f, 0.08f , 15,
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS),
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));
};

void Factory::factoryLegCreate() 
{
        legPos[0] = 0.0f;
        legPos[1] = -0.8f;
        legPos[2] = -0.5f;
		cylinderLegs.push_back(CylinderData(legPos, 0.6f, 0.3f , 15,
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS),
		Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)));
};

void Factory::factoryWallsCreate() 
{
	
	//top wall
    objectPos[0] = 0.0f;
    objectPos[1] = 0.5f - 1.0f/5.0f;
    objectPos[2] = -0.5f;
    walls.push_back(BoxData(objectPos, 1.0f, 1.0f/28.0f, 1.0f, 0));

    //bottom wall
    objectPos[0] = 0.0f;
    objectPos[1] = -0.5f + 1.0f/5.0f;
    objectPos[2] = -0.5f;
	walls.push_back(BoxData(objectPos, 1.0f, 1.0f/28.0f, 13.0f/14.0f, 0));
	
    //left wall
    objectPos[0] = -0.5f + 1.0f/200.0f;
    objectPos[1] = 0.0f;
    objectPos[2] = -0.5f;
    walls.push_back(BoxData(objectPos, 1.0f/70.0f, 4.0f/7.0f, 13.0f/14.0f, 0, Box::Direction::LEFT));

    //right wall
    objectPos[0] = +0.5f - 1.0f/200.0f;;
    objectPos[1] = 0.0f;
    objectPos[2] = -0.5f;
    walls.push_back(BoxData(objectPos, 1.0f/70.0f, 4.0f/7.0f, 13.0f/14.0f, 0, Box::Direction::RIGHT));
	
	//rear wall
    objectPos[0] = 0.0f;
    objectPos[1] = 0.0f;
    objectPos[2] = -1.0f + 1.0f/28.0f;
    walls.push_back(BoxData(objectPos, 7.0f/7.0f, 4.0f/7.0f, 1.0f/70.0f, 0, Box::Direction::REAR));
        
};

void Factory::createLines() 
{
	//rear line
    objectPos[0] = 0.0f;
    objectPos[1] = -0.25f;
    objectPos[2] = -0.3f;
    lines.push_back(AssemblyLine(objectPos, 10, 4, 0.8f, dx));

	//front line legs
    beltLegsCreate(0, true, false);
    beltLegsCreate(0, false, false);

    //front line
    objectPos[0] = 0.0f;
    objectPos[1] = -0.25f;
    objectPos[2] = -0.3f + 1.0f + 2.25f * lines[0].getRadius();
    lines.push_back(AssemblyLine(objectPos, 10, 4, 0.8f, dx));

	//front line legs
    beltLegsCreate(1, true, true);
    beltLegsCreate(1, true, false);
    beltLegsCreate(1, false, true);
    beltLegsCreate(1, false, false);
	
        
};