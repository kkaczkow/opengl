#include "ComplexObjects/Terrain.h"

Terrain::Terrain()
{
}



Terrain* Terrain::loadTerrain(const char* fileName)
{
	
	Terrain* terrain = new Terrain();
	
	 terrain->pHeightMap = SOIL_load_image
		(
		fileName,
		&terrain->iWidth,
		&terrain->iHeight,
		&terrain->iChannels,
		SOIL_LOAD_L
		);

	
	 terrain->texture = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_BOTTOM);

	if (!terrain->pHeightMap)
		cerr << "Heightmap loading error." << endl;

	terrain->initializeHeightMapData();
	return terrain;
}

void Terrain::renderTerrain()
{

	//Save current matrix
	glPushMatrix();

	//Enable/disable features
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);

	//Just in case we set all vertices to white
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	//Move the render space to the correct position (translate)
	glTranslatef(-SKYBOX_SIZE, -SKYBOX_SIZE/2, -SKYBOX_SIZE);

    glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, texture);

    glClientActiveTextureARB(GL_TEXTURE0_ARB);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, fHeightMapTexture);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_FLOAT, 0, fHeightMapNormals);

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, fHeightMapVertices);

	for (int i = 0; i < (2 * SKYBOX_SIZE / STEP_SIZE); ++i)
		glDrawElements(GL_TRIANGLE_STRIP, iHeightMapIndicesCount / (2 * SKYBOX_SIZE / STEP_SIZE), GL_UNSIGNED_SHORT, nHeightMapIndices + i * (2 * SKYBOX_SIZE / STEP_SIZE + 1) * sizeof(GLushort));

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	//Load saved matrix
	glPopAttrib();
	glPopMatrix();
}

void Terrain::initializeHeightMapData()
{
	iHeightMapVerticesCount = (2 * SKYBOX_SIZE / STEP_SIZE + 1) * (2 * SKYBOX_SIZE / STEP_SIZE + 1);
	iHeightMapIndicesCount = 2 * (2 * SKYBOX_SIZE / STEP_SIZE + 1) * 2 * SKYBOX_SIZE / STEP_SIZE;

	nHeightMapIndices  = new GLushort[iHeightMapIndicesCount];
	fHeightMapVertices = new GLfloat[iHeightMapVerticesCount * 3];
	fHeightMapNormals  = new GLfloat[iHeightMapVerticesCount * 3];
	fHeightMapTexture  = new GLfloat[iHeightMapVerticesCount * 2];

	int offset = 0;
	for (int X = 0; X <= (2 * SKYBOX_SIZE); X += STEP_SIZE)
	{
		for (int Z = 0; Z <= (2 * SKYBOX_SIZE); Z += STEP_SIZE)
		{
			fHeightMapVertices[offset] = (float)X;
			fHeightMapVertices[offset + 1] = heightAt(X, Z);
			fHeightMapVertices[offset + 2] = (float)Z;
			offset += 3;
		}
	}

	offset = 0;
	for (int X = 0; X <= (2 * SKYBOX_SIZE); X += STEP_SIZE)
	{
		for (int Z = 0; Z <= (2 * SKYBOX_SIZE); Z += STEP_SIZE)
		{
			if (offset >= iHeightMapIndicesCount)
				break;
			nHeightMapIndices[offset] = (X / STEP_SIZE + 1) * (2 * SKYBOX_SIZE / STEP_SIZE + 1) + Z / STEP_SIZE;
			nHeightMapIndices[offset + 1] = X / STEP_SIZE * (2 * SKYBOX_SIZE / STEP_SIZE + 1) + Z / STEP_SIZE;
			offset += 2;
		}
	}

	offset = 0;
	for (int X = 0; X <= (2 * SKYBOX_SIZE); X += STEP_SIZE)
	{
		for (int Z = 0; Z <= (2 * SKYBOX_SIZE); Z += STEP_SIZE)
		{
			if (offset >= iHeightMapVerticesCount * 2)
				break;
			fHeightMapTexture[offset] = ((float)X / (2 * SKYBOX_SIZE));
			fHeightMapTexture[offset + 1] = ((float)Z / (2 * SKYBOX_SIZE));
			offset += 2;
		}
	}
	bruteForceNormalCalculation(fHeightMapVertices, iHeightMapVerticesCount, nHeightMapIndices, iHeightMapIndicesCount, fHeightMapNormals);
}

float Terrain::heightAt(int X, int Z)                    //This returns the height from a height map index
{
	int x = (int)(X * iWidth / (2 * SKYBOX_SIZE));
	int z = (int)(Z * iHeight / (2 * SKYBOX_SIZE));
	
	if (x < 0)
		x = 0;
	if (x >= iWidth)
		x = iWidth - 1;
	if (z < 0)
		z = 0;
	if (z >= iHeight)
		z = iHeight - 1;

	if (!pHeightMap)                                     //Make sure our data is valid
		return 0;
	return (float)(MOD * pHeightMap[x + (z * iWidth)]);  //Index into our heightat array and return the height
}

void Terrain::bruteForceNormalCalculation(GLfloat* v, int vcount, GLushort* ix, int count, GLfloat* n)
{
	//zero
	for (int i = 0; i < vcount * 3; i++)
	{
		n[i] = 0.0f;
	}
	//for each quad
	for (int i = 0; i < count; i += 4)
	{
		float nx, ny, nz;
		//calculate normal
		//we cheat! let's close our eyes and imagine, that our quad is really a triangle

		//variable names compatible with Wikipedia entry on cross product:)

		float a1 = v[ix[i + 1] * 3 + 0] - v[ix[i] * 3 + 0];
		float a2 = v[ix[i + 1] * 3 + 1] - v[ix[i] * 3 + 1];
		float a3 = v[ix[i + 1] * 3 + 2] - v[ix[i] * 3 + 2];

		float b1 = v[ix[i + 2] * 3 + 0] - v[ix[i] * 3 + 0];
		float b2 = v[ix[i + 2] * 3 + 1] - v[ix[i] * 3 + 1];
		float b3 = v[ix[i + 2] * 3 + 2] - v[ix[i] * 3 + 2];

		//cross product
		nx = a2 * b3 - a3 * b2;
		ny = a3 * b1 - a1 * b3;
		nz = a1 * b2 - a2 * b1;

		//normalize it, so that each quad contributes equally...
		float il = 1.0f / sqrt(nx * nx + ny * ny + nz * nz);
		nx *= il;
		ny *= il;
		nz *= il;

		//accumulate
		n[ix[i] * 3 + 0] += nx;
		n[ix[i] * 3 + 1] += ny;
		n[ix[i] * 3 + 2] += nz;

		n[ix[i + 1] * 3 + 0] += nx;
		n[ix[i + 1] * 3 + 1] += ny;
		n[ix[i + 1] * 3 + 2] += nz;

		n[ix[i + 2] * 3 + 0] += nx;
		n[ix[i + 2] * 3 + 1] += ny;
		n[ix[i + 2] * 3 + 2] += nz;

		n[ix[i + 3] * 3 + 0] += nx;
		n[ix[i + 3] * 3 + 1] += ny;
		n[ix[i + 3] * 3 + 2] += nz;
	}
}
