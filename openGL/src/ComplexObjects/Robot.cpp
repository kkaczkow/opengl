#include "common/common.h"
#include "ComplexObjects/Robot.h"
#include <tuple>
#include <cstdlib>

Robot::Robot(GLfloat *middlePoint, GLfloat speed, GLfloat boxSize)
{
    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];

    this->dx = speed;
    srand(time(NULL));
    this->maxHeight = 0.5f;
    this->armHeight = this->maxHeight;
    this->scanSlideMax = -boxSize;
    this->scanSlide = 0.0f;
    this->rotation = 0.0f;
    this->rotationMax = 90.0f;
    GLfloat cylRot[] = {0.0f, 0.0f, 0.0f};
    this->running = false;
    this->rdyToTakeBox = false;
    this->armRange = -1.0f - (1.0f/7.0f)*boxSize - boxSize;
    this->phase = RobotPhase::IDLE;
    this->dest = Destinations::UNSPECIFIED;
    this->da = (180.0f*dx)/(3.14f*glm::abs(armRange));
    boxPos[2] = -0.5f;

    //box reaching arm
    armBoxes.push_back
		(
        BoxData(
        boxPos,
        (2.0f/7.0f)*boxSize,
        (2.0f/7.0f)*boxSize,
        1.0f)
        );

    //"hand"
    boxPos[0] = 0.0f;
    boxPos[1] = 0.0f;
    boxPos[2] = -1.0f - (1.0f/14.0f)*boxSize;
    armBoxes.push_back
		(
        BoxData(
        boxPos,
        (9.0f/7.0f)*boxSize,
        (5.0f/7.0f)*boxSize,
        (1.0f/7.0f)*boxSize)
        );

    //left finger
    boxPos[0] = - boxSize/2.0f - (1.0f/14.0f)*boxSize; 
    boxPos[1] = 0.0f;
    boxPos[2] = armRange + boxSize/2.0f;
    armBoxes.push_back
		(
        BoxData(
        boxPos,
        (1.0f/7.0f)*boxSize,
        (5.0f/7.0f)*boxSize,
        boxSize)
        );

    //right finger
    boxPos[0] = boxSize/2.0f + (1.0f/14.0f)*boxSize; 
    boxPos[1] = 0.0f;
    boxPos[2] = armRange + boxSize/2.0f;
    armBoxes.push_back
		(
        BoxData(
        boxPos,
        (1.0f/7.0f)*boxSize,
        (5.0f/7.0f)*boxSize,
        boxSize)
        );

    for(auto &b : armBoxes) 
	{
        b.setTexture(Utils::TextureManager::getTexture(Utils::TexID::ROBOT_ARM));
    }
	
    //BASE arm
    boxPos[0] = boxPos[1] = boxPos[2] = 0.0f;
    baseArm = BoxData(boxPos,(4.0f/7.0f)*boxSize,2.0f,(4.0f/7.0f)*boxSize);
    baseArm.setTexture(Utils::TextureManager::getTexture(Utils::TexID::ROBOT_ARM));
	
	//middle cylinder
	boxPos[0] = boxPos[2] = 0.0f;
	boxPos[1] = -0.4f;
    midCylinder = CylinderData(boxPos, 0.5, 0.35, 20, 0, 0, cylRot);
    midCylinder.setTexture
		(
        Utils::TextureManager::getTexture(Utils::TexID::METAL_WHEEL),
        Utils::TextureManager::getTexture(Utils::TexID::R2D2_CENTER)
		);

	//base cylinder
    boxPos[1] = -0.8f;
    baseCylinder = CylinderData(boxPos, 0.5, 0.4, 20, 0, 0, cylRot);
    baseCylinder.setTexture
		(
        Utils::TextureManager::getTexture(Utils::TexID::METAL_WHEEL),
        Utils::TextureManager::getTexture(Utils::TexID::R2D2_BOTTOM)
		);

    //arm cylinder
    boxPos[1] = -0.1f;
    armCylinder = CylinderData(boxPos, 0.2, 0.3, 20, 0, 0, cylRot);
    armCylinder.setTexture
		(
        Utils::TextureManager::getTexture(Utils::TexID::METAL_WHEEL),
        Utils::TextureManager::getTexture(Utils::TexID::R2D2_TOP)
		);
	
    //SCAN ARM
    //rotation cylinder
    boxPos[0] = boxPos[2] = 0.0f;
    boxPos[1] = 1.0f;
    cylRot[0] = cylRot[1] = 0.0f;
    cylRot[2] = 90.0f;
    //scanCylis.push_back(CylinderData(boxPos, (4.0f/7.0f)*boxSize, 0.25, 20, 0, 0, cylRot));
	
    //scanning cylinder
    boxPos[0] = boxPos[2] = 0.0f;
    boxPos[1] = 1.0f + 1.0f + (3.0f/2.0f)*boxSize + (1.0f/7.0f)*boxSize;
    cylRot[0] = cylRot[1] = cylRot[2] = 0.0f;
    scanCylinder = CylinderData(boxPos, (1.0f/7.0f)*boxSize, 0.25, 20, 0, 0, cylRot);
    //arm
    boxPos[0] = boxPos[2] = 0.0f;
    boxPos[1] = 1.0f + 0.75f/2.0f;
    scanBoxes.push_back(BoxData(boxPos,(3.0f/7.0f)*boxSize, 0.75f, (3.0f/7.0f)*boxSize));
    //sliding arm
    boxPos[0] = boxPos[2] = 0.0f;
    boxPos[1] = 1.0f + 0.75f + boxSize;
    scanBoxes.push_back(BoxData(boxPos,(2.0f/7.0f)*boxSize, 0.75f, (2.0f/7.0f)*boxSize));

    for(auto &b : scanBoxes) 
	{
        b.setTexture(Utils::TextureManager::getTexture(Utils::TexID::ROBOT_ARM));
    }

    scanCylinder.setTexture
		(
           Utils::TextureManager::getTexture(Utils::TexID::METAL_WHEEL),
           Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS)
        );
    

}


Robot::~Robot(void)
{
}

void Robot::display(void) 
{
	
	midCylinder.display();
	baseCylinder.display();

    glPushMatrix();
    glTranslatef(middlePoint[0], middlePoint[1], middlePoint[2]);
    glRotatef(rotation, 0.0f, 1.0f, 0.0f);

    glPushMatrix();
    {

        glTranslatef(0.0f, armHeight, 0.0f);
        for(auto &p : armBoxes) 
		{
            p.display();
        }

        if(product != nullptr) 
		{
            if(phase == RobotPhase::GO_UP || phase == RobotPhase::RETURN) 
			{
                glTranslatef(0.0f, -armHeight, 0.0f);
			}

            product->display();
        }
    }
    glPopMatrix();

    baseArm.display();
    armCylinder.display();

    glPushMatrix();
    {
        glTranslatef(0.0f, 1.0f, 0.0f);
        glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
        glTranslatef(0.0f, -1.0f, 0.0f);
		boxPos[1] = 1.0f;
		
        scanBoxes[0].display();
		
        //display sliding scan parts
        glTranslatef(0.0f, scanSlide, 0.0f);
        GLfloat light_position[] = { 0.0, scanCylinder.getMiddlePoint()[1], -maxHeight, 1.0 };
        //GLfloat light_position[] = { 0.0, 0.9, 0.0, 1.0 };
        GLfloat laser_direction[] = {0.0f, 0.0f, -1.0f, 0.0f};
        glLightfv(GL_LIGHT2, GL_POSITION, light_position);
        glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, laser_direction);
        scanBoxes[1].display();
		scanCylinder.display();
		drawSphere(0.2, boxPos);
		
	}
    glPopMatrix();

    
    glPopMatrix();
}

void Robot::step(void) 
{

    if(!running)
	{ 
		return;
	}
}

void Robot::step(GLboolean boxRdy)
{

    if(boxRdy && phase == RobotPhase::IDLE) 
	{
        robotPhaseObjArrived();
    }

    switch(phase) 
	{
    case RobotPhase::SCAN:
        robotPhaseScan();
        break;
    case RobotPhase::SCAN_RETURN:
        robotPhaseScanReturn();
        break;
    case RobotPhase::PICK:
        robotPhasePick();
        break;
    case RobotPhase::ROTATE:
        robotPhaseRotate();
        break;
    case RobotPhase::PUT:
        robotPhaseBeltPut();
        break;
    case RobotPhase::GO_UP:
        robotPhaseUp();
        break;
    case RobotPhase::RETURN:
        robotPhaseReturn();
        break;
    default:
        break;
    }

}

void Robot::placeProduct(std::shared_ptr<BoxData> box) 
{
    product = box;
    GLfloat *p = box->getMiddlePoint();
    p[2] = armRange;

    product->setMiddlePoint(p);
}

void Robot::robotPhaseObjArrived(void) 
{
    armHeight -= dx;

    if(armHeight < 0.0f) {
        armHeight = 0.0f;
        glEnable(GL_LIGHT2);
        phase = RobotPhase::SCAN;
        robotPhaseScan();
    }
}

void Robot::robotPhaseScan(void) 
{
    scanSlide -= dx;

    if(scanSlide < scanSlideMax) 
	{
        scanSlide = scanSlideMax;
        phase = RobotPhase::SCAN_RETURN;
        robotPhaseScanReturn();
    }
}

void Robot::robotPhaseScanReturn(void) 
{
    scanSlide += dx;

    if(scanSlide > 0.0f) {
        scanSlide = 0.0f;
        glDisable(GL_LIGHT2);
        phase = RobotPhase::PICK;
        robotPhasePick();
    }
}

void Robot::robotPhasePick(void) 
{
    armHeight += dx;

    if(armHeight > maxHeight) 
	{
        armHeight = maxHeight;
        //decide where to go with box
        phase = RobotPhase::ROTATE;
        dest = (Destinations)(std::rand() % 3);
        robotPhaseRotate();
    }
}

void Robot::robotPhaseRotate(void) 
{
    switch(dest) 
	{
    case Destinations::RIGHT_BELT:
        rotation -= da;
        if(rotation < -rotationMax) 
		{
            rotation = -rotationMax;
            phase = RobotPhase::PUT;
            robotPhaseBeltPut();
        }
        break;
    case Destinations::LEFT_BELT:
        rotation += da;
        if(rotation > rotationMax) 
		{
            rotation = rotationMax;
            phase = RobotPhase::PUT;
            robotPhaseBeltPut();
        }
        break;

	case Destinations::REAR_BELT:
        rotation += 2*da;
        if(rotation > 2*rotationMax) 
		{
            rotation = 2*rotationMax;
            phase = RobotPhase::PUT;
            robotPhaseBeltPut();
        }
        break;
    
    default:
        break;
    }
}

void Robot::robotPhaseBeltPut(void) 
{
    armHeight -= dx;

    if(armHeight < 0.0f) 
	{
        armHeight = 0.0f;
        phase = RobotPhase::GO_UP;
        robotPhaseUp();
    }
}

void Robot::robotPhaseUp(void) 
{
    armHeight += dx;

    if(armHeight > maxHeight) 
	{
        armHeight = maxHeight;
        phase = RobotPhase::RETURN;
        //armPhaseUp();
    }
}

void Robot::robotPhaseReturn(void) 
{
    switch(dest) {
    case Destinations::RIGHT_BELT:
        rotation += da;
        if(rotation > 0.0f) 
		{
            rotation = 0.0f;
            phase = RobotPhase::IDLE;
        }
        break;
    case Destinations::LEFT_BELT:
        rotation -= da;
        if(rotation < 0.0f) 
		{
            rotation = 0.0f;
            phase = RobotPhase::IDLE;
        }
        break;

	case Destinations::REAR_BELT:
        rotation -= 2*da;
        if(rotation < 0.0f) 
		{
            rotation = 0.0f;
            phase = RobotPhase::IDLE;
        }
        break;
    
    default:
        break;
    }
}

GLboolean Robot::isBoxRdyToTake(void) const 
{
	return phase == RobotPhase::RETURN &&
		(dest == Destinations::LEFT_BELT || dest == Destinations::RIGHT_BELT || dest == Destinations::REAR_BELT) &&
		product != nullptr ? true : false;
}

std::shared_ptr<BoxData> Robot::removeProduct(void) 
{
	auto ptr = product; 
    product = nullptr; 
    return ptr;
}

std::shared_ptr<BoxData> Robot::getProduct(void) const 
{ 
	return product;
}

Robot::Destinations Robot::getDestination(void) const 
{
	return dest;
}

GLboolean Robot::canTransferBoxToArm(void) const 
{
	return phase == RobotPhase::PICK ? true : false;
}

GLboolean Robot::isIdle(void) const 
{
	return phase == RobotPhase::IDLE ? true : false;
}

GLfloat Robot::getArmRange(void) const 
{
	return glm::abs(armRange);
}

void Robot::drawSphere(GLfloat Radius, GLfloat *middlePoint)
{
	glPushMatrix(); //remember current matrix

		glTranslatef(middlePoint[0],middlePoint[1], middlePoint[2]);

		glBindTexture(GL_TEXTURE_2D, 
			Utils::TextureManager::getTexture(Utils::TexID::METAL_LEGS));

		glBegin( GL_LINE_LOOP );
		GLUquadricObj *quadric;
		quadric = gluNewQuadric();

		gluQuadricDrawStyle(quadric, GLU_FILL );
		gluSphere( quadric , Radius , 36 , 18 );

		gluDeleteQuadric(quadric); 
		glEndList();

		glEnd();

	glPopMatrix();

}
