#include "Primitives/SkyBox.h"

Skybox::Skybox() {}

Skybox* Skybox::loadSkybox()
{
	Skybox* skybox = new Skybox();
	for (int i = 0; i < 6; i++)
	{

		glGenTextures(1, skybox->tTextures + i);
		
		switch(i)
		{
		case 0:
			skybox->tTextures[i] = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_RIGHT);
			break;
			
		case 1:
			skybox->tTextures[i] = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_LEFT);
			break;
			
		case 2:
			skybox->tTextures[i] = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_TOP);
			break;
			
		case 3:
			skybox->tTextures[i] = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_BOTTOM);
			break;
			
		case 4:
			skybox->tTextures[i] = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_FRONT);
			break;
			
		case 5:
			skybox->tTextures[i] = Utils::TextureManager::getTexture(Utils::TexID::SKYBOX_BACK);
			break;
		}

		//Typical texture generation using data from the bitmap
        glActiveTextureARB(GL_TEXTURE0_ARB);
		glBindTexture(GL_TEXTURE_2D, skybox->tTextures[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	}
	return skybox;
}

void Skybox::draw()
{
	//Save current matrix
	glPushMatrix();

	//Enable/disable features
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);

	//Begin draw skybox
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

	//Second move the render space to the correct position (translate)
	//glTranslatef(position.x, position.y, position.z);

	//First apply scale matrix
	glScalef(SKYBOX_SIZE, SKYBOX_SIZE, SKYBOX_SIZE);

	//Common axis x - right face
	drawTexturedRect('x', true, tTextures[0]);

	//Common axis x - left face
	drawTexturedRect('x', false, tTextures[1]);

	//Common axis y - top face
	drawTexturedRect('y', true, tTextures[2]);

	//Common axis y - bottom face
	drawTexturedRect('y', false, tTextures[3]);

	//Common axis z - front face
	drawTexturedRect('z', true, tTextures[4]);

	//Common axis z - back face
	drawTexturedRect('z', false, tTextures[5]);

	//Load saved matrix
	glPopAttrib();
	glPopMatrix();

    glEnable(GL_LIGHTING);
}

void Skybox::drawTexturedRect(char axis, bool positive, GLuint texture)
{
	float p = 1.0f;
	float z = 0.0f;
	float n = -1.0f;
	float m;
	if (positive)
		m = 1.0f;
	else
		m = -1.0f;

    
    glActiveTextureARB(GL_TEXTURE0_ARB);
	glBindTexture(GL_TEXTURE_2D, texture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin(GL_QUADS);

	//Common axis x - right & left faces
	if (axis == 'x')
	{
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,z, z); glVertex3f(m * p, n, m * p); //lower left
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,p, z); glVertex3f(m * p, n, m * n); //lower right
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,p, p); glVertex3f(m * p, p, m * n); //upper right
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,z, p); glVertex3f(m * p, p, m * p); //upper left
	}

	//Common axis y - top & bottom faces
	else if (axis == 'y')
	{
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,z, z); glVertex3f(m * n, m * p, p);
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,p, z); glVertex3f(m * p, m * p, p);
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,p, p); glVertex3f(m * p, m * p, n);
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,z, p); glVertex3f(m * n, m * p, n);
	}

	//Common axis z - front & back faces
	else if (axis == 'z')
	{
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,z, z); glVertex3f(m * n, n, m * p);
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,p, z); glVertex3f(m * p, n, m * p);
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,p, p); glVertex3f(m * p, p, m * p);
		glMultiTexCoord2fARB(GL_TEXTURE0_ARB,z, p); glVertex3f(m * n, p, m * p);
	}
	glEnd();

	glDisable(GL_BLEND);
}
