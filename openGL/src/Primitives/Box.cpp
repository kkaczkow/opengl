#include "Primitives/Box.h"

GLfloat Box::vertices[][3] = {
    {-0.5f,  0.5f, -0.5f},//left top rear
    { 0.5f,  0.5f, -0.5f},//right top rear 
    { 0.5f,  0.5f,  0.5f},//right top front 
    {-0.5f,  0.5f,  0.5f},//left top front 
    {-0.5f, -0.5f,  0.5f},//left bottom front 
    {-0.5f, -0.5f, -0.5f},//left bottom rear
    { 0.5f, -0.5f, -0.5f},//right bottom rear 
    { 0.5f, -0.5f,  0.5f} //right bottom front 
}; 

GLfloat Box::normals[][3] = {
    { 0.0f,  0.0f,  1.0f},//FRONT
    { 1.0f,  0.0f,  0.0f},//RIGHT
    {-1.0f,  0.0f,  0.0f},//LEFT
    { 0.0f,  0.0f, -1.0f},//REAR
    { 0.0f,  1.0f,  0.0f},//TOP
    { 0.0f, -1.0f,  0.0f} //BOTTOM
}; 

GLfloat Box::rotations[][4] = {
    { 90.0f,  1.0f,  0.0f, 0.0f},//FRONT
    { -90.0f,  0.0f,  0.0f, 1.0f},//RIGHT
    {  90.0f,  0.0f,  0.0f, 1.0f},//LEFT
    { -90.0f,  1.0f,  0.0f, 0.0f},//REAR
    { 0.0f,  1.0f,  0.0f, 0.0f},//TOP
    { 180.0f, 0.0f,  0.0f, 1.0f} //BOTTOM
}; 

GLint Box::slices = 4;

void Box::draw() {
    GLfloat step = 1.0f/(GLfloat)slices;

    for(int i = 0; i < slices; ++i) {
        for(int j = 0; j < slices; ++j) {
            glBegin(GL_QUADS);
            drawTopSide(step, i, j);
            drawBottomSide(step, i, j);
            drawLeftSide(step, i, j);
            drawRightSide(step, i, j);
            drawFrontSide(step, i, j);
            drawRearSide(step, i, j);
            glEnd();
        }
    }
}

void Box::draw(Direction direction) {
    GLfloat step = 1.0f/(GLfloat)slices;

	for(int i = 0; i < slices; ++i) {
        for(int j = 0; j < slices; ++j) {

            glBegin(GL_QUADS);

			switch(direction) 
			{

			case Direction::TOP:
				drawTopSide(step, i, j);
				drawLeftSide(step, i, j);
				drawRightSide(step, i, j);
				drawFrontSide(step, i, j);
				drawRearSide(step, i, j);
				break;

			case Direction::BOTTOM:
				drawBottomSide(step, i, j);
				drawLeftSide(step, i, j);
				drawRightSide(step, i, j);
				drawFrontSide(step, i, j);
				drawRearSide(step, i, j);
				break;

			case Direction::LEFT:
				drawTopSide(step, i, j);
				drawBottomSide(step, i, j);
				drawLeftSide(step, i, j);
				drawFrontSide(step, i, j);
				drawRearSide(step, i, j);
				break;

			case Direction::RIGHT:
				drawTopSide(step, i, j);
				drawBottomSide(step, i, j);
				drawRightSide(step, i, j);
				drawFrontSide(step, i, j);
				drawRearSide(step, i, j);
				break;

			case Direction::FRONT:
				drawTopSide(step, i, j);
				drawBottomSide(step, i, j);
				drawLeftSide(step, i, j);
				drawRightSide(step, i, j);
				drawFrontSide(step, i, j);
				break;

			case Direction::REAR:
				drawTopSide(step, i, j);
				drawBottomSide(step, i, j);
				drawLeftSide(step, i, j);
				drawRightSide(step, i, j);
				drawRearSide(step, i, j);
				break;



			}
			
            glEnd();
		}
    }
}

void Box::drawLeftSide(const GLfloat step, const int i, const int j) 
{
	glNormal3fv(normals[(int)Direction::LEFT]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)(j+1));
	glVertex3f(vertices[0][0], -step*(GLfloat)(j+1) + vertices[0][1], step*(GLfloat)i + vertices[0][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)j);
	glVertex3f(vertices[0][0], -step*(GLfloat)j + vertices[0][1], step*(GLfloat)i + vertices[0][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)j);
	glVertex3f(vertices[0][0], -step*(GLfloat)j + vertices[0][1], step*(GLfloat)(i+1) + vertices[0][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)(j+1));
	glVertex3f(vertices[0][0], -step*(GLfloat)(j+1) + vertices[0][1], step*(GLfloat)(i+1) + vertices[0][2]);
}

void Box::drawRightSide(const GLfloat step, const int i, const int j) 
{
	glNormal3fv(normals[(int)Direction::RIGHT]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)(j+1));
	glVertex3f(vertices[2][0], -step*(GLfloat)(j+1) + vertices[2][1], -step*(GLfloat)i + vertices[2][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)j);
	glVertex3f(vertices[2][0], -step*(GLfloat)j + vertices[2][1], -step*(GLfloat)i + vertices[2][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)j);
	glVertex3f(vertices[2][0], -step*(GLfloat)j + vertices[2][1], -step*(GLfloat)(i+1) + vertices[2][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)(j+1));
	glVertex3f(vertices[2][0], -step*(GLfloat)(j+1) + vertices[2][1], -step*(GLfloat)(i+1) + vertices[2][2]);
}

void Box::drawTopSide(const GLfloat step, const int i, const int j) 
{
	glNormal3fv(normals[(int)Direction::TOP]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)(j+1));
	glVertex3f(step*(GLfloat)(j+1) + vertices[0][0], vertices[0][1], step*(GLfloat)i + vertices[0][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)j);
	glVertex3f(step*(GLfloat)j + vertices[0][0], vertices[0][1], step*(GLfloat)i + vertices[0][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)j);
	glVertex3f(step*(GLfloat)j + vertices[0][0], vertices[0][1], step*(GLfloat)(i+1) + vertices[0][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)(j+1));
	glVertex3f(step*(GLfloat)(j+1) + vertices[0][0], vertices[0][1], step*(GLfloat)(i+1) + vertices[0][2]);
}

void Box::drawBottomSide(const GLfloat step, const int i, const int j) 
{
	glNormal3fv(normals[(int)Direction::BOTTOM]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)(j+1));
	glVertex3f(step*(GLfloat)(j+1) + vertices[4][0], vertices[4][1], -step*(GLfloat)i + vertices[4][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)j);
	glVertex3f(step*(GLfloat)j + vertices[4][0], vertices[4][1], -step*(GLfloat)i + vertices[4][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)j);
	glVertex3f(step*(GLfloat)j + vertices[4][0], vertices[4][1], -step*(GLfloat)(i+1) + vertices[4][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)(j+1));
	glVertex3f(step*(GLfloat)(j+1) + vertices[4][0], vertices[4][1], -step*(GLfloat)(i+1) + vertices[4][2]);
}

void Box::drawFrontSide(const GLfloat step, const int i, const int j) 
{
	glNormal3fv(normals[(int)Direction::FRONT]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)(j+1));
	glVertex3f(step*(GLfloat)i + vertices[3][0], -step*(GLfloat)(j+1) + vertices[3][1], vertices[3][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)j);
	glVertex3f(step*(GLfloat)i + vertices[3][0], -step*(GLfloat)j + vertices[3][1], vertices[3][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)j);
	glVertex3f(step*(GLfloat)(i+1) + vertices[3][0], -step*(GLfloat)j + vertices[3][1], vertices[3][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)(j+1));
	glVertex3f(step*(GLfloat)(i+1) + vertices[3][0], -step*(GLfloat)(j+1) + vertices[3][1], vertices[3][2]);
}

void Box::drawRearSide(const GLfloat step, const int i, const int j) 
{
	glNormal3fv(normals[(int)Direction::REAR]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)(j+1));
	glVertex3f(-step*(GLfloat)i + vertices[1][0], -step*(GLfloat)(j+1) + vertices[1][1], vertices[1][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)i, step * (GLfloat)j);
	glVertex3f(-step*(GLfloat)i + vertices[1][0], -step*(GLfloat)j + vertices[1][1], vertices[1][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)j);
	glVertex3f(-step*(GLfloat)(i+1) + vertices[1][0], -step*(GLfloat)j + vertices[1][1], vertices[1][2]);
	glMultiTexCoord2fARB(GL_TEXTURE0_ARB,step * (GLfloat)(i+1), step * (GLfloat)(j+1));
	glVertex3f(-step*(GLfloat)(i+1) + vertices[1][0], -step*(GLfloat)(j+1) + vertices[1][1], vertices[1][2]);
}