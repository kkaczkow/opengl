#include "Primitives/PrimitiveDescriptors/CylinderData.h"


CylinderData::CylinderData(void)
{
    this->middlePoint[0] = 0.0f;
    this->middlePoint[1] = 0.0f;
    this->middlePoint[2] = 0.0f;

    this->height = 1.0f;
    this->radius = 1.0f;

    this->baseTex = this->sideTex = 0;


    this->selfRotation[0] = 0.0f;
    this->selfRotation[1] = 0.0f;
    this->selfRotation[2] = 0.0f;

    initMats();
}

CylinderData::CylinderData(GLfloat *middlePoint, GLfloat height, GLfloat radius, GLint slices, GLint baseTex, GLint sideTex)
{
    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];

    this->height = height;
    this->radius = radius;
    this->slices = slices;

    this->baseTex = baseTex; 
    this->sideTex = sideTex;

    initMats();
}

CylinderData::CylinderData(GLfloat *middlePoint, GLfloat height, GLfloat radius, GLint slices, GLint baseTex, GLint sideTex, GLfloat *rotation)
{
    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];

    this->height = height;
    this->radius = radius;
    this->slices = slices;

    this->baseTex = baseTex; 
    this->sideTex = sideTex;

    this->selfRotation[0] = rotation[0];
    this->selfRotation[1] = rotation[1];
    this->selfRotation[2] = rotation[2];

    initMats();
}

CylinderData::~CylinderData(void)
{
}

void CylinderData::initMats(void) {
    this->matAmbient[0] = this->matAmbient[1] = this->matAmbient[2] = 0.2f;
    this->matAmbient[3] = 1.0f;

    this->matDiffuse[0] = this->matDiffuse[1] = this->matDiffuse[2] = 0.8f;
    this->matDiffuse[3] = 1.0f;

    this->matSpecular[0] = this->matSpecular[1] = this->matSpecular[2] = 0.0f;
    this->matSpecular[3] = 1.0f;

    this->matEmission[0] = this->matEmission[1] = this->matEmission[2] = 0.0f;
    this->matEmission[3] = 1.0f;

    this->matShininess = 0.0f;
}

void CylinderData::display(void) {
    glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
    glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
    glMaterialf(GL_FRONT, GL_SHININESS, matShininess);

    glPushMatrix();
    {
        glTranslatef(middlePoint[0], middlePoint[1], middlePoint[2]);
        glRotatef(selfRotation[0], 1.0f, 0.0f, 0.0f);
        glRotatef(selfRotation[1], 0.0f, 1.0f, 0.0f);
        glRotatef(selfRotation[2], 0.0f, 0.0f, 1.0f);
        glScalef(1.0f, height, 1.0f);
        Cylinder::draw(slices, radius, baseTex, sideTex);
    }
    glPopMatrix();
}

void CylinderData::setTexture(GLint base, GLint side) {
    this->baseTex = base; 
    this->sideTex = side;
}

void CylinderData::setMiddlePoint(GLfloat *point) {
    this->middlePoint[0] = point[0];
    this->middlePoint[1] = point[1];
    this->middlePoint[2] = point[2];
}

float* const CylinderData::getMiddlePoint() {
    return middlePoint;
}

void CylinderData::setSelfRotation(GLfloat *point) {
    this->selfRotation[0] = point[0];
    this->selfRotation[1] = point[1];
    this->selfRotation[2] = point[2];
}

float* const CylinderData::getSelfRotation() {
    return selfRotation;
}

