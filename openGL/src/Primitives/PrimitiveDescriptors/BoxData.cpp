#include "Primitives/PrimitiveDescriptors/BoxData.h"


BoxData::BoxData(void)
{
    init();
}

BoxData::BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth)
{
    init(middlePoint, width, height, depth);
	isPane = false;
}

BoxData::BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture)
{
    init(middlePoint, width, height, depth, texture);
	isPane = false;
}

BoxData::BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture, Box::Direction direction)
{
    init(middlePoint, width, height, depth, texture);
	BoxData::direction = direction;
	isPane = true;
}

BoxData::BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture, GLfloat *rotation)
{
    init(middlePoint, width, height, depth, texture, rotation);
	isPane = false;
}

BoxData::~BoxData(void)
{
}

void BoxData::display(void) 
{

    glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
    glMaterialfv(GL_FRONT, GL_EMISSION, matEmission);
    glMaterialf(GL_FRONT, GL_SHININESS, matShininess);

    glActiveTextureARB(GL_TEXTURE0_ARB);
    glBindTexture(GL_TEXTURE_2D, tex);
    glPushMatrix();
    {
        glTranslatef(middlePoint[0], middlePoint[1], middlePoint[2]);
        glRotatef(selfRotation[0], 1.0f, 0.0f, 0.0f);
        glRotatef(selfRotation[1], 0.0f, 1.0f, 0.0f);
        glRotatef(selfRotation[2], 0.0f, 0.0f, 1.0f);
        glScalef(width, height, depth);
		if(isPane) 
		{
			 Box::draw(direction);
		}
		else
		{
			 Box::draw();
		}

    }
    glPopMatrix();
}

void BoxData::setTexture(GLint tex) 
{
    this->tex = tex;
}

void BoxData::init(void) 
{
    this->middlePoint[0] = 0.0f;
    this->middlePoint[1] = 0.0f;
    this->middlePoint[2] = 0.0f;

    this->width = 1.0f;
    this->height = 1.0f;
    this->depth = 1.0f;

    this->tex = 0;

    this->selfRotation[0] = 0.0f;
    this->selfRotation[1] = 0.0f;
    this->selfRotation[2] = 0.0f;

    this->matAmbient[0] = this->matAmbient[1] = this->matAmbient[2] = 0.2f;
    this->matAmbient[3] = 1.0f;

    this->matDiffuse[0] = this->matDiffuse[1] = this->matDiffuse[2] = 0.8f;
    this->matDiffuse[3] = 1.0f;

    this->matSpecular[0] = this->matSpecular[1] = this->matSpecular[2] = 0.0f;
    this->matSpecular[3] = 1.0f;

    this->matEmission[0] = this->matEmission[1] = this->matEmission[2] = 0.0f;
    this->matEmission[3] = 1.0f;

    this->matShininess = 0.0f;
}

void BoxData::init(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth) 
{
    init();

    this->middlePoint[0] = middlePoint[0];
    this->middlePoint[1] = middlePoint[1];
    this->middlePoint[2] = middlePoint[2];

    this->width = width;
    this->height = height;
    this->depth = depth;
}

void BoxData::init(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture) 
{
    init(middlePoint, width, height, depth);

    this->tex = texture;
}

void BoxData::init(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture, GLfloat *rotation) 
{
    init(middlePoint, width, height, depth, texture);

    this->selfRotation[0] = rotation[0];
    this->selfRotation[1] = rotation[1];
    this->selfRotation[2] = rotation[2];
}

void BoxData::setMiddlePoint(GLfloat *point) 
{
    this->middlePoint[0] = point[0];
    this->middlePoint[1] = point[1];
    this->middlePoint[2] = point[2];
}

float* const BoxData::getMiddlePoint() 
{
    return middlePoint;
}

void BoxData::setSelfRotation(GLfloat *point) 
{
    this->selfRotation[0] = point[0];
    this->selfRotation[1] = point[1];
    this->selfRotation[2] = point[2];
}

float* const BoxData::getSelfRotation() 
{
    return selfRotation;
}
