#include "Primitives\Cylinder.h"

const GLfloat Cylinder::normals[][3] = {
    { 0.0f,  1.0f,  0.0f},//TOP
    { 0.0f, -1.0f,  0.0f}//BOTTOM
};

glm::vec4 Cylinder::next;
glm::vec4 Cylinder::cur;

void Cylinder::draw(GLint sides, GLfloat radius, GLint baseTex, GLint sideTex) {
    next[0] = radius;
    next[1] = 0.5f;
    next[2] = 0.0f;
    next[3] = 1.0f;
    cur = next;

    glm::vec2 texCur(0.5f, 0.0f);
    glm::vec2 texNext = texCur;

    GLfloat sidesf = (GLfloat)sides;
    GLfloat angle = 360.0f/sidesf;

    for(int i = 0; i < sides; ++i) {
        //for each slice draw top, bottom and wall
        next = glm::rotateY(next, angle);
        texNext = glm::rotate(texNext, angle);

        glActiveTextureARB(GL_TEXTURE0_ARB);
        glBindTexture(GL_TEXTURE_2D, baseTex);

        glBegin(GL_TRIANGLES);
			//top wall triangle
			glNormal3fv(normals[0]);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,0.5f, 0.5f);glVertex3f(0.0f, 0.5f, 0.0f);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,texCur.x+0.5f, texCur.y+0.5f);glVertex3f(cur.x, cur.y, cur.z);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,texNext.x+0.5f, texNext.y+0.5f);glVertex3f(next.x, next.y, next.z);
			//bottom wall triangle
			glNormal3fv(normals[1]);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,0.5f, 0.5f);glVertex3f(0.0f, -0.5f, 0.0f);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,texCur.x+0.5f, texCur.y+0.5f);glVertex3f(cur.x, -cur.y, cur.z);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,texNext.x+0.5f, texNext.y+0.5f);glVertex3f(next.x, -next.y, next.z);

        glEnd();

        glBindTexture(GL_TEXTURE_2D, sideTex);

        glBegin(GL_QUADS);
			//side wall
			glNormal3f(next.x, 0.0f, next.z);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,(GLfloat)(i+1)/sidesf, 1.0f);glVertex3f(next.x, next.y, next.z);//top next
			glNormal3f(cur.x, 0.0f, cur.z);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,(GLfloat)(i)/sidesf, 1.0f);glVertex3f(cur.x, cur.y, cur.z);//top current
			glNormal3f(cur.x, 0.0f, cur.z);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,(GLfloat)(i)/sidesf, 0.0f);glVertex3f(cur.x, -cur.y, cur.z);//bottom current
			glNormal3f(next.x, 0.0f, next.z);
			glMultiTexCoord2fARB(GL_TEXTURE0_ARB,(GLfloat)(i+1)/sidesf, 0.0f);glVertex3f(next.x, -next.y, next.z);//bottom next

        glEnd();

        cur = next;
        texCur = texNext;
    }
}