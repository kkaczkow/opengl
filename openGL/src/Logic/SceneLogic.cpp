#include "Logic/SceneLogic.h"
#include <thread>

SceneLogic *SceneLogic::instance = SceneLogic::makeInst();
float SceneLogic::maxRot = 10.0f;
float SceneLogic::maxSpeed = 2.0f;
float SceneLogic::rotAcc = 0.5f;
float SceneLogic::speedAcc = 0.5f;

SceneLogic *SceneLogic::makeInst(void) 
{
    auto ptr = new SceneLogic();

    for(int i = 0; i < 256; ++i) 
	{
        ptr->keys[i] = false;
    }
	ptr->lmb = false;
	ptr->start_x = 0.0f;
	ptr->start_y = 0.0f;
	ptr->curr_x = 0.0f;
	ptr->curr_y = 0.0f;
	ptr->start_horizontal_angle = 0.0f;
	ptr->start_vertical_angle = 0.0f;

	ptr->curSpeed = 0.0f;
	ptr->curXRotSpeed = 0.0f;
	ptr->curYRotSpeed = 0.0f;



    return ptr;
}

void SceneLogic::keyDownFunc(unsigned char key, int x, int y) 
{
    SceneLogic::getInstance()->keys[key] = true;
}

void SceneLogic::keyUpFunc(unsigned char key, int x, int y) 
{
    SceneLogic::getInstance()->keys[key] = false;
}

void SceneLogic::animStep(void) 
{
    SceneLogic *sl = SceneLogic::getInstance();
    Scene *sc = Scene::getInstance();

    sl->processMove();
    sc->factory->step();
    
    for(auto &b : sc->belts) 
	{
        b->step();
    }

    sc->robot->step(sc->factory->isBoxRdyToTake());

    if(sc->robot->canTransferBoxToArm() && sc->robot->getProduct() == nullptr) 
	{
        sc->robot->placeProduct(sc->factory->removeProduct());
    }

    if(sc->robot->isBoxRdyToTake()) 
	{
        auto ptr = sc->robot->removeProduct();

        if(sc->robot->getDestination() == Robot::Destinations::REAR_BELT) 
		{
            sc->belts[0]->putProduct(ptr);
        }
		else if(sc->robot->getDestination() == Robot::Destinations::RIGHT_BELT) 
		{
            sc->belts[1]->putProduct(ptr);
        }
		else if(sc->robot->getDestination() == Robot::Destinations::LEFT_BELT)
		{
			sc->belts[2]->putProduct(ptr);
		}
    }

    Scene::display();

    for(int i = 0; i < 256; ++i) 
	{
        SceneLogic::getInstance()->keys[i] = false;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(20));
}

void SceneLogic::createBox(void) 
{
    Scene::getInstance()->factory->createProduct();
}

void SceneLogic::mouseKey(int button, int state, int x, int y) 
{ 
	SceneLogic *sl = SceneLogic::getInstance();
	auto sc = Scene::getInstance();
	
    if (button == GLUT_LEFT_BUTTON) 
        if(state == GLUT_DOWN) 
        { 
            sl->start_x = sl->curr_x = x; 
            sl->start_y = sl->curr_y = y; 
            sl->lmb = true; 
        } 
        else
        { 
            sl->start_horizontal_angle = sc->curr_horizontal_angle; 
            sl->start_vertical_angle = sc->curr_vertical_angle; 
            sl->lmb = false; 
        } 
} 
  
void SceneLogic::mouseMove(int x, int y) 
{ 
	SceneLogic *sl = SceneLogic::getInstance();

    if(sl->lmb) 
    { 
        sl->curr_x = x; 
        sl->curr_y = y; 
    }
} 

void SceneLogic::processMove() 
{ 
	auto sc = Scene::getInstance();

	if(keys['p'])
	{
		sc->camera_type_free = !sc->camera_type_free;
	}

	// first type of camera
	if(sc->camera_type_free)
	{
		if(lmb)
		{ 
			sc->curr_horizontal_angle = start_horizontal_angle - (toRad((float)(start_x - curr_x)/2.0f)); 
			sc->curr_vertical_angle = start_vertical_angle + toRad((float)(start_y - curr_y)/2.0f); 
			if(sc->curr_vertical_angle >= M_PI/2) 
			{
				sc->curr_vertical_angle = (float)(M_PI/2-0.01); 
			}
			if(sc->curr_vertical_angle <= -M_PI/2) 
			{
				sc->curr_vertical_angle = (float)(-(M_PI/2-0.01)); 
			}
		} 
		if(keys['w'] && !keys['s']) 
		{ 
			sc->x_pos += (float)sc->MOVE_SPEED*sin(sc->curr_horizontal_angle)*cos(sc->curr_vertical_angle); 
			sc->y_pos += (float)sc->MOVE_SPEED*sin(sc->curr_vertical_angle); 
			sc->z_pos -= (float)sc->MOVE_SPEED*cos(sc->curr_horizontal_angle)*cos(sc->curr_vertical_angle); 
		} 
		if(!keys['w'] && keys['s']) 
		{ 
			sc->x_pos -= (float)sc->MOVE_SPEED*sin(sc->curr_horizontal_angle)*cos(sc->curr_vertical_angle); 
			sc->y_pos -= (float)sc->MOVE_SPEED*sin(sc->curr_vertical_angle); 
			sc->z_pos += (float)sc->MOVE_SPEED*cos(sc->curr_horizontal_angle)*cos(sc->curr_vertical_angle); 
		} 
		if(keys['a'] && !keys['d']) 
		{ 
			sc->x_pos -= (float)sc->MOVE_SPEED*cos(sc->curr_horizontal_angle); 
			sc->z_pos -= (float)sc->MOVE_SPEED*sin(sc->curr_horizontal_angle); 
		} 
		if(!keys['a'] && keys['d']) 
		{ 
			sc->x_pos += (float)sc->MOVE_SPEED*cos(sc->curr_horizontal_angle); 
			sc->z_pos += (float)sc->MOVE_SPEED*sin(sc->curr_horizontal_angle); 
		}

		if(sc->x_pos < -SKYBOX_SIZE+2) 
		{
			sc->x_pos = -SKYBOX_SIZE+2; 
		}
		if(sc->x_pos > SKYBOX_SIZE-2) 
		{
			sc->x_pos = SKYBOX_SIZE-2; 
		}
		if(sc->z_pos < -SKYBOX_SIZE+2) 
		{
			sc->z_pos = -SKYBOX_SIZE+2; 
		}
		if(sc->z_pos > SKYBOX_SIZE-2) 
		{
			sc->z_pos = SKYBOX_SIZE-2; 
		}

		if(sc->y_pos < sc -> terrain ->heightAt((int)sc->x_pos+SKYBOX_SIZE, (int)sc->z_pos+SKYBOX_SIZE) - 121.0f)
		{
			sc->y_pos = sc -> terrain ->heightAt((int)sc->x_pos+SKYBOX_SIZE, (int)sc->z_pos+SKYBOX_SIZE) - 121.0f;
		}
		if(sc->y_pos > SKYBOX_SIZE-2) 
		{
			sc->y_pos = SKYBOX_SIZE-2; 
		}

	}
	// second type of camera
	else
	{
		if(keys['a'] ^ keys['d']) 
		{
			if(keys['a']) 
			{
				curYRotSpeed += rotAcc;
				if(curYRotSpeed > maxRot)
				{
					curYRotSpeed = maxRot;
				}
			}
			else if(keys['d']) 
			{
				curYRotSpeed -= rotAcc;
				if(curYRotSpeed < -maxRot)
				{
					curYRotSpeed = -maxRot;
				}
			}
		}
		else 
		{
			if(curYRotSpeed < 0.0f) 
			{
				curYRotSpeed += rotAcc;
				if(curYRotSpeed > 0.0f)
				{
					curYRotSpeed = 0.0f;
				}
			}
			else if (curYRotSpeed > 0.0f) 
			{
				curYRotSpeed -= rotAcc;
				if(curYRotSpeed < 0.0f)
				{
					curYRotSpeed = 0.0f;
				}
			}
		}

		if(keys['w'] ^ keys['s']) 
		{
			if(keys['w']) 
			{
				curXRotSpeed += rotAcc;
				if(curXRotSpeed > maxRot)
				{
					curXRotSpeed = maxRot;
				}
			}
			else if(keys['s']) 
			{
				curXRotSpeed -= rotAcc;
				if(curXRotSpeed < -maxRot)
				{
					curXRotSpeed = -maxRot;
				}
			}
		}
		else 
		{
			if(curXRotSpeed < 0.0f) 
			{
				curXRotSpeed += rotAcc;
				if(curXRotSpeed > 0.0f)
				{
					curXRotSpeed = 0.0f;
				}
			}
			else if (curXRotSpeed > 0.0f)
			{
				curXRotSpeed -= rotAcc;
				if(curXRotSpeed < 0.0f)
				{
					curXRotSpeed = 0.0f;
				}
			}
		}

		if(keys['q'] ^ keys['e']) 
		{
			if(keys['q']) 
			{
				curSpeed += speedAcc;
				if(curSpeed > maxSpeed)
				{
					curSpeed = maxSpeed;
				}
			}
			else if(keys['e']) 
			{
				curSpeed -= speedAcc;
				if(curSpeed < -maxSpeed)
				{
					curSpeed = -maxSpeed;
				}
			}
		}
		else 
		{
			if(curSpeed < 0.0f) 
			{
				curSpeed += speedAcc;
				if(curSpeed > 0.0f)
				{
					curSpeed = 0.0f;
				}
			}
			else if (curSpeed > 0.0f) 
			{
				curSpeed -= speedAcc;
				if(curSpeed < 0.0f)
				{
					curSpeed = 0.0f;
				}
			}
		}

		auto sc = Scene::getInstance();


		if(sc->distance > SKYBOX_SIZE)
		{
			sc->distance = SKYBOX_SIZE;
		}
		else if(sc->distance < -SKYBOX_SIZE)
		{
			sc->distance = -SKYBOX_SIZE;
		}
	}

	sc->xRot += curXRotSpeed;
	sc->yRot += curYRotSpeed;
	sc->distance += curSpeed;

	if(keys['z']) 
	{
		SceneLogic::getInstance()->createBox();
		keys['z'] = false;
	}

	if(keys['l']) 
	{
		sc->doShadows = !sc->doShadows;
		keys['l'] = false;
	}

	if(keys['m']) 
	{
		sc->doFog = !sc->doFog;
		keys['m'] = false;
	}

    
} 

float SceneLogic::toRad(float x)
{
	return (float)(x*M_PI/180);
}