#include "common/common.h"

void Utils::triangleNormal(GLfloat *p1, GLfloat *p2, GLfloat *p3, GLfloat *result) {
    //vertexes in counter clockwise order to front of triangle
    GLfloat V[] = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
    GLfloat W[] = {p3[0] - p1[0], p3[1] - p1[1], p3[2] - p1[2]};

    result[0] = (V[1] * W[2]) - (V[2] * W[1]);
    result[1] = (V[2] * W[0]) - (V[0] * W[2]);
    result[2] = (V[0] * W[1]) - (V[1] * W[0]);

    //TODO: normalize
}

void Utils::polygonNormal(const std::vector<GLfloat*> &p, GLfloat *result) {
    result[0] = result[1] = result[2] = 0.0f;

    GLfloat current[3];
    GLfloat next[3];
    int nextIdx;

    for(unsigned i = 0; i < p.size(); ++i) {
        current[0] = (p[i])[0];
        current[1] = (p[i])[1];
        current[2] = (p[i])[2];

        nextIdx = (i + 1) % p.size();

        next[0] = (p[nextIdx])[0];
        next[1] = (p[nextIdx])[1];
        next[2] = (p[nextIdx])[2];

        result[0] += (current[1] - next[1]) * (current[2] + current[2]);
        result[1] += (current[2] - next[2]) * (current[0] + current[0]);
        result[2] += (current[0] - next[0]) * (current[1] + current[1]);
    }

    //TODO: normalize
}

// Function load a image, turn it into a texture, and return the texture ID as a GLuint for use
GLuint Utils::loadImage(const char* theFileName)
{
	ILuint imageID;				// Create an image ID as a ULuint
 
	GLuint textureID;			// Create a texture ID as a GLuint
 
	ILboolean success;			// Create a flag to keep track of success/failure
 
	ILenum error;				// Create a flag to keep track of the IL error state
 
	ilGenImages(1, &imageID); 		// Generate the image ID
 
	ilBindImage(imageID); 			// Bind the image
 
	success = ilLoadImage((ILstring)theFileName); 	// Load the image file
 
	// If we managed to load the image, then we can start to do things with it...
	if (success)
	{
		// If the image is flipped (i.e. upside-down and mirrored, flip it the right way up!)
		ILinfo ImageInfo;
		iluGetImageInfo(&ImageInfo);
		if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT)
		{
			iluFlipImage();
		}
 
		// Convert the image into a suitable format to work with
		// NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
		success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
 
		// Quit out if we failed the conversion
		if (!success)
		{
			error = ilGetError();
			//std::cout << "Image conversion failed - IL reports error: " << error << " - " << iluErrorString(error) << std::endl;
			exit(-1);
		}
 
		// Generate a new texture
		glGenTextures(1, &textureID);
 
		// Bind the texture to a name
		glBindTexture(GL_TEXTURE_2D, textureID);
 
		// Set texture clamping method
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
 
		// Set texture interpolation method to use linear interpolation (no MIPMAPS)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        //ilutGLBuildMipmaps();

        ILint tak = ilGetInteger(IL_IMAGE_FORMAT);
 
		// Specify the texture specification
		glTexImage2D(GL_TEXTURE_2D, 				// Type of texture
					 0,				// Pyramid level (for mip-mapping) - 0 is the top level
					 ilGetInteger(IL_IMAGE_FORMAT),	// Internal pixel format to use. Can be a generic type like GL_RGB or GL_RGBA, or a sized type
					 ilGetInteger(IL_IMAGE_WIDTH),	// Image width
					 ilGetInteger(IL_IMAGE_HEIGHT),	// Image height
					 0,				// Border width in pixels (can either be 1 or 0)
					 ilGetInteger(IL_IMAGE_FORMAT),	// Format of image pixel data
					 GL_UNSIGNED_BYTE,		// Image data type
					 ilGetData());			// The actual image data itself

       	}
  	else // If we failed to open the image file in the first place...
  	{
		error = ilGetError();
		//std::cout << "Image load failed - IL reports error: " << error << " - " << iluErrorString(error) << std::endl;
		exit(-1);
  	}
 
 	ilDeleteImages(1, &imageID); // Because we have already copied image data into texture data we can release memory used by image.
 
	//std::cout << "Texture creation successful." << std::endl;
 
	return textureID; // Return the GLuint to the texture so you can use it!
}