#pragma once

#include "Primitives\Cylinder.h"
 
//TODO: Texture coords are not correct
struct CylinderData
{
    GLfloat middlePoint[3];
    GLfloat height;
    GLfloat radius;
    GLfloat selfRotation[3];
    GLint slices;
    GLint baseTex;
    GLint sideTex;

    GLfloat matAmbient[4];
    GLfloat matDiffuse[4];
    GLfloat matSpecular[4];
    GLfloat matEmission[4];
    GLfloat matShininess;

    CylinderData(void);
    CylinderData(GLfloat *middlePoint, GLfloat height, GLfloat radius, GLint slices, GLint baseTex, GLint sideTex, GLfloat *rotation);
    CylinderData(GLfloat *middlePoint, GLfloat height, GLfloat radius, GLint slices, GLint baseTex, GLint sideTex);
	~CylinderData(void);
private:
    void initMats(void);
public:
    void setTexture(GLint base, GLint side);
    void setMiddlePoint(GLfloat *point);
    GLfloat* const getMiddlePoint();
    void setSelfRotation(GLfloat *point);
    GLfloat* const getSelfRotation();

    void display(void);
};

