#pragma once

#include "Primitives\Box.h"

struct BoxData
{
    GLfloat middlePoint[3];
    GLfloat height;
    GLfloat width;
    GLfloat depth;
    GLfloat selfRotation[3];
    GLint tex;
    
    GLfloat matAmbient[4];
    GLfloat matDiffuse[4];
    GLfloat matSpecular[4];
    GLfloat matEmission[4];
    GLfloat matShininess;
	Box::Direction direction;
	GLboolean isPane;

    BoxData(void);
    BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth);
    BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture);
	BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture, Box::Direction direction);
    BoxData(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture, GLfloat *rotation);
    ~BoxData(void);

private:
    void init(void);
    void init(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth);
    void init(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture);
    void init(GLfloat *middlePoint, GLfloat width, GLfloat height, GLfloat depth, GLint texture, GLfloat *rotation);

public:
    void setTexture(GLint tex);
    void setMiddlePoint(GLfloat *point);
    GLfloat* const getMiddlePoint();
    void setSelfRotation(GLfloat *point);
    GLfloat* const getSelfRotation();

    void display(void);
};

