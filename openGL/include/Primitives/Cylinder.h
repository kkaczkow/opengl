#pragma once
#include "common/common.h"
class Cylinder
{
    static const GLfloat normals[][3];
    static glm::vec4 next;
    static glm::vec4 cur;
    Cylinder(void);
public:
    static void draw(GLint sides, GLfloat radius, GLint baseTex, GLint sideTex);
};

