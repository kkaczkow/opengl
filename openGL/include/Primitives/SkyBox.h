#pragma once

#ifndef GL_CLAMP_TO_EDGE 
#define	GL_CLAMP_TO_EDGE	0x812F 
#endif

#include "common/common.h"
#include <iostream>
#include <string>
#include <iostream>
#include <string>
#include "Utils/TextureManager.h"

#define SKYBOX_SIZE 250


class Skybox
{
public:
	static Skybox* loadSkybox();
	void draw();
	void drawTexturedRect(char axis, bool positive, GLuint texture);

private:
	Skybox();
	GLuint tTextures[6];
};