#pragma once
#include "common/common.h"

class Box
{//defines box with edge length of 1
    static GLfloat vertices[8][3];
    static GLfloat normals[6][3];
    static GLfloat rotations[6][4];
    static GLint slices;

    Box(void);
	static void drawLeftSide(const GLfloat step, const int i, const int j);
	static void drawRightSide(const GLfloat step, const int i, const int j);
	static void drawTopSide(const GLfloat step, const int i, const int j);
	static void drawBottomSide(const GLfloat step, const int i, const int j);
	static void drawFrontSide(const GLfloat step, const int i, const int j);
	static void drawRearSide(const GLfloat step, const int i, const int j);

public:
	enum class Direction {
        FRONT = 0,
        RIGHT,
        LEFT,
        REAR,
        TOP,
        BOTTOM
    };

    static void draw(void);
	static void draw(Direction direction);
};

