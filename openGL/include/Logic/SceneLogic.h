#pragma once

#include "common/common.h"
#include "Application/Scene.h"

class SceneLogic
{
    SceneLogic(void) {}
    ~SceneLogic(void) {}
    static SceneLogic *makeInst(void);
    static SceneLogic *instance;

	//camera data - first type
	float start_x;
	float start_y;
	float curr_x;
	float curr_y;
	float start_horizontal_angle;
	float start_vertical_angle;

	// camera data - second type
	static float maxRot;
	static float maxSpeed;
	static float rotAcc;
	static float speedAcc;
	float curXRotSpeed;
	float curYRotSpeed;
	float curSpeed;

    bool keys[256];
	bool lmb;

	void processMove(void);
	

public:
    static SceneLogic *getInstance(void) { return instance; } 
    static void keyDownFunc(unsigned char key, int x, int y);
    static void keyUpFunc(unsigned char key, int x, int y);
	static void mouseKey(int button, int state, int x, int y);
	static void mouseMove(int x, int y);
    static void animStep(void);
	static float toRad(float x);

    void createBox(void);
};


