#pragma once

#include "common/common.h"
#include "Primitives/PrimitiveDescriptors/BoxData.h"
#include "Primitives/PrimitiveDescriptors/CylinderData.h"
#include "ComplexObjects/AssemblyLine.h"
#include <deque>

class Factory
{
    GLfloat middlePoint[3];
    GLfloat dx;

    GLint productTex;
    GLboolean rdyToTakeBox;
	GLboolean running;

	float objectPos[3];
	float legPos[3];
	float legRot[3];

    std::vector<BoxData> walls;
    std::vector<AssemblyLine> lines;
	std::vector<CylinderData> cylinderLegs;
	std::vector<BoxData> boxLegs;
    std::deque<std::shared_ptr<BoxData>> products;
    
public:
    Factory(float *middlePoint, GLfloat speed);
    ~Factory(void);

    void createProduct(void);
    std::shared_ptr<BoxData> getProduct(void);
    std::shared_ptr<BoxData> removeProduct(void);
    void setMainWallTex(GLint texID);
	void setSideWallTex(GLint texID);
    void setLineTex(GLint cylinderBaseTex, GLint cylinderSideTex, GLint plankTexID);
    void setProductTex(GLint texID);
    GLboolean isBoxRdyToTake(void) const {return rdyToTakeBox;}
    GLfloat getBoxSize(void) const {return 2.0f * (glm::abs( -lines[0].getMiddlePoint()[1]) - lines[0].getSurface());}
    void setMiddlePoint(GLfloat *p) {for(int i = 0;i<3;++i) middlePoint[i] = p[i];}
    GLfloat getFrontBeltEndPoint(void) {return lines[1].getMiddlePoint()[2] + 1.0f/2.0f;}
	void beltLegsCreate (unsigned i, bool leftSide, bool rearCylinder);
	void factoryLegCreate();
	void factoryWallsCreate();
	void createLines();
    void display(void);
    void step(void);
};

