#pragma once

#include "common/common.h"
#include "Primitives/PrimitiveDescriptors/BoxData.h"
#include "Primitives/PrimitiveDescriptors/CylinderData.h"
#include <tuple>

class AssemblyLine
{

    enum class BoxPhase {
        PHASE1 = 0,
        PHASE2,
        PHASE3,
        PHASE4
    };

    typedef std::tuple<BoxData, GLfloat, BoxPhase> BoxTuple;
    typedef std::tuple<CylinderData> CylinderTuple;

    GLfloat middlePoint[3];
    GLfloat length;
    GLfloat dx;
    GLfloat da;
    GLfloat r;
    GLfloat surface;//distance from middle point to surface
	GLboolean running;

    std::vector<BoxTuple> data;
    std::vector<CylinderTuple> cylis;

    void boxPhase1(BoxTuple &data);
    void boxPhase2(BoxTuple &data);
    void boxPhase3(BoxTuple &data);
    void boxPhase4(BoxTuple &data);
    
public:
    AssemblyLine(GLfloat *middlePoint, unsigned upperPlanks, unsigned arcPlanks, GLfloat fill, GLfloat speed);
    ~AssemblyLine(void);

    void display(void);
    void step(void);
    const GLfloat *getMiddlePoint(void) const {return middlePoint;}
    GLfloat getRadius() const {return r;}
    GLfloat getSurface() const {return surface;}
    GLfloat *getFrontCylinder() {auto &p = cylis[1];return std::get<0>(p).getMiddlePoint(); }
    GLfloat *getRearCylinder() {return std::get<0>(cylis[0]).getMiddlePoint(); }
    void setCylinderTexure(GLint baseTex, GLint sideTex);
    void setPlankTexture(GLint texID);
};

