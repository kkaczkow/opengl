#pragma once

#include "common/common.h"
#include "Primitives/SkyBox.h"

#define MOD			0.65
#define STEP_SIZE	10

using namespace std;

class Terrain
{
public:
	static Terrain* loadTerrain(const char* fileName);
	void renderTerrain();
	float heightAt(int X, int Z);

private:
	Terrain();
	void initializeHeightMapData();
	void bruteForceNormalCalculation(GLfloat* v, int vcount, GLushort* ix, int count, GLfloat* n);
	int iWidth;
	int iHeight;
	int iChannels;
	int iHeightMapIndicesCount;
	int iHeightMapVerticesCount;
	GLuint texture;
	GLubyte* pHeightMap;
	GLushort* nHeightMapIndices;
	GLfloat* fHeightMapVertices;
	GLfloat* fHeightMapNormals;
	GLfloat* fHeightMapTexture;
};
