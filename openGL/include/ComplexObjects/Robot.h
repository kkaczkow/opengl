#pragma once

#include "common/common.h"
#include "Primitives/PrimitiveDescriptors/BoxData.h"
#include "Primitives/PrimitiveDescriptors/CylinderData.h"

class Robot
{
    GLfloat middlePoint[3];
    GLfloat dx;
    GLfloat da;

    GLfloat rotation; //self rotation of whole object
    GLfloat rotationMax; 


    GLfloat armHeight; //height of arm holding box
    GLfloat maxHeight; //max height arm can go up

    GLfloat scanSlide;
    GLfloat scanSlideMax;

    GLfloat armRange; //how far arm can reach
    
    //GLfloat boxFallHeight;

    GLint productTex;
    GLboolean rdyToTakeBox;
	GLboolean running;

	float boxPos[3];

public:
    enum class RobotPhase {
        IDLE = 0,
        OBJ_RDY,
        SCAN,
        SCAN_RETURN,
        PICK,
        ROTATE,
        PUT,
        GO_UP,
        RETURN
    };

    enum class Destinations {
        LEFT_BELT = 0,
        RIGHT_BELT,
		REAR_BELT,
		UNSPECIFIED
    };

private:
    RobotPhase phase;
    Destinations dest;


    //box holder
    std::vector<BoxData> armBoxes;
    //base
    BoxData baseArm;
    CylinderData armCylinder;
	CylinderData midCylinder;
	CylinderData baseCylinder;

    //box scanner
    CylinderData scanCylinder;
    std::vector<BoxData> scanBoxes;


    std::shared_ptr<BoxData> product;

    void robotPhaseObjArrived(void);
    void robotPhaseScan(void);
    void robotPhaseScanReturn(void);
    void robotPhasePick(void);
    void robotPhaseRotate(void);
    void robotPhaseBeltPut(void);
    void robotPhaseUp(void);
    void robotPhaseReturn(void);
    
public:
    Robot(float *middlePoint, GLfloat speed, GLfloat boxSize);
    ~Robot(void);
    void placeProduct(std::shared_ptr<BoxData> box);
    std::shared_ptr<BoxData> getProduct(void) const;
    std::shared_ptr<BoxData> removeProduct(void);
    Destinations getDestination(void) const;
    GLboolean canTransferBoxToArm(void) const;
    GLboolean isBoxRdyToTake(void) const;
    GLboolean isIdle(void) const;
    GLfloat getArmRange(void) const;

    void display(void);
    void step(void);
    void step(GLboolean boxRdy);
	void drawSphere(GLfloat radius, GLfloat *middlePoint);
};

