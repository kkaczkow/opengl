#pragma once

#include "common/common.h"
#include "Primitives/PrimitiveDescriptors/BoxData.h"
#include "ComplexObjects/AssemblyLine.h"
#include <deque>

class Belt
{

    float middlePoint[3];
    float dx;
	float linePos[3];
	float legPos[3];
	GLboolean running;
	GLboolean ifRightLine;
	GLboolean ifLeftLine;

    std::vector<AssemblyLine> lines;
	std::vector<CylinderData> cylinderLegs;
	std::vector<BoxData> boxLegs;
    std::deque<std::shared_ptr<BoxData>> products;
    
public:
    Belt(float *middlePoint, GLfloat speed, GLboolean ifRightLine, GLboolean ifLeftLine);
    ~Belt(void);

    void putProduct(std::shared_ptr<BoxData> b);
    void removeProduct(void);
	void beltLegsCreate (unsigned i, bool leftSide, bool rearCylinder);

    void display(void);
    void step(void);
    void setLineTex(GLint cylinderBaseTex, GLint cylinderSideTex, GLint plankTexID);
    void setMiddlePoint(GLfloat *p) {for(int i = 0;i<3;++i) middlePoint[i] = p[i];}
    GLfloat getLengthToEndPoint(void);
};

