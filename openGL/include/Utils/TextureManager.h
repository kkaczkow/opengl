#pragma once

#include "common/common.h"
#include <map>

#define SMAP 1024

namespace Utils{

    enum class TexID {
        METAL_WHEEL,
		WHEEL,
        METAL_LEGS,
		LINE,
        PRODUCT,
		R2D2_TOP,
		R2D2_CENTER,
		R2D2_BOTTOM,
        METAL_SIDE,
        FACTORY_WALL,
		FACTORY_SIDE_WALL,
        ROBOT_ARM,
		SKYBOX_LEFT,
		SKYBOX_RIGHT,
		SKYBOX_TOP,
		SKYBOX_BOTTOM,
		SKYBOX_FRONT,
		SKYBOX_BACK,
		FLOOR,
        DEPTH
    };

    class TextureManager
    {
        static const std::string defaultTextures[];
        static std::map<TexID,GLint> textures;

        TextureManager(void);
    public:
		
        static void init(void);
        static void loadTexture(TexID id, const char* fileName);
        static void loadAll(void);
        static GLint getTexture(TexID id);
        static void createDepthTexture();
    };

}