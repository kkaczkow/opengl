#pragma once

#include "common/common.h"
#include "ComplexObjects/Belt.h"
#include "ComplexObjects/Factory.h"
#include "ComplexObjects/AssemblyLine.h"
#include "ComplexObjects/Robot.h"
#include "ComplexObjects/Terrain.h"
#include "Logic/SceneLogic.h"
#include "Primitives/SkyBox.h"

//defines a scene holding all objects that are
//to be displayed
class Scene
{
    typedef std::shared_ptr<Factory> FactoryPtr;
    typedef std::shared_ptr<Belt> BeltPtr;
    typedef std::shared_ptr<Robot> RobotPtr;

    friend class SceneLogic;

    Scene(void) {}
    ~Scene(void) {}
    static Scene *makeInst(void);
    static Scene *instance;

	float coords[3];

    //camera data - first type
	bool camera_type_free;
	GLfloat x_pos;
	GLfloat y_pos;
	GLfloat z_pos;
	GLfloat MOVE_SPEED;
	GLfloat curr_horizontal_angle;
	GLfloat curr_vertical_angle;
	// camera data - second type
	GLfloat xRot;
    GLfloat yRot;
    GLfloat distance;

    FactoryPtr factory;
    std::vector<BeltPtr> belts;
    RobotPtr robot;
	Skybox* skybox;
	Terrain* terrain;

    //matrices
    GLfloat lightProjection[16];
    GLfloat lightView[16];
    GLfloat cameraProjection[16];
    GLfloat cameraView[16];
    GLfloat lightPosition[4];

    //GL_LIGHT1 properties
    GLfloat light_ambient[4];
    GLfloat light_specular[4];
    GLfloat light_diffuse[4];
    //GL_LIGHT1 dim properties
    GLfloat whiteMat[4];
    GLfloat blackMat[4];
	
	bool doShadows;
	bool doFog;

    void setDimLight(void);
    void setNormalLight(void);
    void initMatrices(void);

public:

    static GLsizei width;
    static GLsizei height;

    static Scene *getInstance(void) { return instance; } 

    static void display(void);
    static void reshape(GLsizei w, GLsizei h);

    void init(void);
    void displayObjects(void);
    void drawCoordLines(void);

    void displayFromLightPOV(void);
    void displayFromCameraPOV(void);
};

