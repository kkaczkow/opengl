#pragma once

#include "GLee/GLee.h"
#include <windows.h>
#include <gl/GL.h>
#include "GLUT.H"
#include "IL/il.h"
#include "IL/ilu.h"
#include "IL/ilut.h"
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "Utils/TextureManager.h"
#include "SOIL.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include <memory>

namespace Utils {
    void triangleNormal(GLfloat *p1, GLfloat *p2, GLfloat *p3, GLfloat *result);
    void polygonNormal(const std::vector<GLfloat*> &p, GLfloat *result);
    GLuint loadImage(const char* theFileName);
}